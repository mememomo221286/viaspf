# viaspf

The **viaspf** library contains a complete implementation of the *Sender Policy
Framework* (SPF) specification, version 1, as described in [RFC 7208]. It
provides an asynchronous API for checking an email sender’s authorisation
according to the specification.

This library implements the core SPF protocol, but it does not directly depend
on a DNS resolver. Instead, users of this library can provide an implementation
of a DNS lookup trait, and so choose themselves which DNS resolver they want to
use to implement their SPF verifier applications.

The implementation uses [Tokio], but for the timeout logic only. If needed, it
is possible to use a different async runtime for driving the library by toggling
certain Cargo features.

This library was first created in a ‘clean room’ setting. It was written from
scratch, referring only to the RFC, and following it to the letter. Extensive
checking ensures correctness and conformance with RFC 7208.

The minimum supported Rust version is 1.61.0.

[RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208
[Tokio]: https://tokio.rs

## Usage

This is a Rust library. Include viaspf in `Cargo.toml` as usual. For ease of
use, you may want to enable the feature `trust-dns-resolver`; see below.

The main purpose of the viaspf library is SPF verification, that is, checking
whether a sending host is authorised to use some mail domain according to
published SPF policy. The function [`evaluate_sender`] is provided as the main
API item.

```rust
use std::net::IpAddr;
use viaspf::*;

let config = Default::default();
let ip = IpAddr::from([1, 2, 3, 4]);
let mail_from = "amy@example.org";

let spf_result = match mail_from.parse() {
    Ok(sender) => {
        evaluate_sender(&resolver, &config, ip, &sender, None)
            .await
            .spf_result
    }
    _ => SpfResult::None,
};

assert_eq!(spf_result, SpfResult::Pass);
```

The example above is straightforward. It demonstrates checking of the MAIL FROM
identity `amy@example.org`.

The first argument, `resolver`, deserves a brief explanation. While viaspf
contains a complete implementation of the SPF protocol, it does not itself
include DNS resolution capabilities. Instead, DNS resolution is abstracted into
the trait [`Lookup`], and is thus ‘pluggable’. Provide an implementation of this
trait using the resolver of your choice to control how DNS queries are done.

As a convenience, the Cargo feature `trust-dns-resolver` can be enabled to make
an implementation of `Lookup` available for the [Trust-DNS] resolver.

Refer to the [API documentation] for details.

[`evaluate_sender`]: https://docs.rs/viaspf/0.5.3/viaspf/fn.evaluate_sender.html
[`Lookup`]: https://docs.rs/viaspf/0.5.3/viaspf/lookup/trait.Lookup.html
[API documentation]: https://docs.rs/viaspf

## Examples

A simple SPF verifier is included as an executable example: the command-line
tool `spfquery`. This program uses the [Trust-DNS] resolver to perform DNS
lookups.

Pass an IP address and a domain name as arguments to `spfquery`. The query is
then evaluated and the result and a trace is printed out.

```
cargo run --features trust-dns-resolver \
  --example spfquery -- 35.190.247.12 example.com
```

```
IP: 35.190.247.12
Domain: example.com
SPF result: pass
Mechanism: mx
Trace:
  executing SPF query for domain "example.com"
  looking up TXT records for "example.com"
  evaluating SPF record "v=spf1 mx -all"
  evaluating directive "mx"
  evaluating mechanism "mx"
  incrementing global lookup count
  using target name "example.com"
  looking up MX records for "example.com"
  trying MX name "mail.example.com"
  incrementing per-mechanism lookup count
  looking up A records for "mail.example.com"
  trying IP address 35.190.247.12
  mechanism matched
  evaluated directive to result "pass"
  evaluated SPF query to result "pass"
```

For interactive use of the viaspf library, please see the command-line utility
[spftrace].

[Trust-DNS]: https://github.com/bluejekyll/trust-dns
[spftrace]: https://crates.io/crates/spftrace

## Licence

Copyright © 2020–2023 David Bürgin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/.
