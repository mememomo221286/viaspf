// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use crate::{record::Mechanism, trace::Trace};
use std::fmt::{self, Display, Formatter};

/// The result of an SPF evaluation.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum SpfResult {
    /// The *none* authorisation result.
    None,
    /// The *neutral* authorisation result.
    Neutral,
    /// The *pass* authorisation result.
    Pass,
    // This variant’s `ExplanationString` argument is not optional due to §6.2:
    // ‘If no "exp" modifier is present, then either a default explanation
    // string or an empty explanation string MUST be returned to the calling
    // application.’
    /// The *fail* authorisation result, containing an explanation of the
    /// failure.
    Fail(ExplanationString),
    /// The *softfail* authorisation result.
    Softfail,
    /// The *temperror* error result.
    Temperror,
    /// The *permerror* error result.
    Permerror,
}

impl Display for SpfResult {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::None => write!(f, "none"),
            Self::Neutral => write!(f, "neutral"),
            Self::Pass => write!(f, "pass"),
            Self::Fail(explanation) => {
                if explanation.as_ref().is_empty() {
                    write!(f, "fail")
                } else {
                    write!(f, "fail ({explanation})")
                }
            }
            Self::Softfail => write!(f, "softfail"),
            Self::Temperror => write!(f, "temperror"),
            Self::Permerror => write!(f, "permerror"),
        }
    }
}

/// An explanation of why a query evaluated to a [`Fail`] result.
///
/// If the explanation string is used in an SMTP response, clients must make
/// sure the contained `String` is limited to ASCII, according to section 6.2 of
/// RFC 7208.
///
/// [`Fail`]: SpfResult::Fail
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ExplanationString {
    // `ExplanationString` is an enum with two variants in order to allow
    // clients to distinguish the two possible origins of the explanation. This
    // distinction is implied in §8.4: ‘The check_host() function will return
    // either a default explanation string or one from the domain that published
    // the SPF records […]. If the information does not originate with the
    // checking software’ …
    /// The default explanation (when no external explanation is available).
    ///
    /// This explanation string may be rendered as an empty or generic message
    /// text.
    Default,
    /// An external explanation, obtained from a domain’s SPF record.
    External(String),
}

impl Default for ExplanationString {
    fn default() -> Self {
        Self::Default
    }
}

impl Display for ExplanationString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl AsRef<str> for ExplanationString {
    fn as_ref(&self) -> &str {
        match self {
            Self::Default => "",
            Self::External(s) => s,
        }
    }
}

/// The result of evaluating an SPF query.
#[derive(Debug)]
pub struct QueryResult {
    /// The SPF result of the query.
    pub spf_result: SpfResult,
    /// Details about the cause that produced the SPF result, if available.
    pub cause: Option<SpfResultCause>,
    /// A trace of events that occurred during evaluation, if available.
    pub trace: Option<Trace>,
}

/// The result of macro-expanding an explain string.
#[derive(Debug)]
pub struct ExpansionResult {
    /// The macro-expanded explain string.
    pub expansion: String,
    /// A trace of events that occurred during expansion, if available.
    pub trace: Option<Trace>,
}

/// The cause that led to an SPF result.
///
/// This enumeration is intended to support reporting information with the
/// `mechanism` and `problem` keys described in section 9.1 of RFC 7208.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum SpfResultCause {
    /// The mechanism that matched in case of an authorisation result.
    Match(Mechanism),
    /// An indication of the error cause in case of an error result.
    Error(ErrorCause),
}

impl From<Mechanism> for SpfResultCause {
    fn from(mechanism: Mechanism) -> Self {
        Self::Match(mechanism)
    }
}

impl From<ErrorCause> for SpfResultCause {
    fn from(error_cause: ErrorCause) -> Self {
        Self::Error(error_cause)
    }
}

/// Causes of an SPF query error result.
///
/// These error causes provide a broad categorisation of an SPF error result
/// that may be shown to users. The `Display` representations are in ASCII and
/// contain no double quotes, such that they are directly suited for SMTP
/// messages, email headers, or similar. More detailed information about error
/// causes can be accessed in the trace.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum ErrorCause {
    /// The query timed out.
    Timeout,
    /// A DNS error occurred.
    Dns,
    /// The lookup limit was exceeded.
    LookupLimitExceeded,
    /// The void lookup limit was exceeded.
    VoidLookupLimitExceeded,
    /// A recursive query found no SPF record at a target domain.
    NoSpfRecord,
    /// Multiple SPF records were found at a target domain.
    MultipleSpfRecords,
    /// An SPF record could not be parsed.
    InvalidSpfRecordSyntax,
    /// An SPF record contained an invalid target domain name.
    InvalidTargetDomain,
}

impl Display for ErrorCause {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        // These error messages are kept in a terse, alphabetic-ASCII-only style
        // suitable for SMTP messages or email headers.
        match self {
            Self::Timeout => write!(f, "DNS lookup timed out"),
            Self::Dns => write!(f, "DNS lookup failed"),
            Self::LookupLimitExceeded => write!(f, "lookup limit exceeded"),
            Self::VoidLookupLimitExceeded => write!(f, "void lookup limit exceeded"),
            Self::NoSpfRecord => write!(f, "no SPF record found at included domain"),
            Self::MultipleSpfRecords => write!(f, "more than one SPF record found"),
            Self::InvalidSpfRecordSyntax => write!(f, "invalid SPF record found"),
            Self::InvalidTargetDomain => write!(f, "invalid included domain name found"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn spf_result_fail_display_empty() {
        let e = ExplanationString::Default;
        assert_eq!(&SpfResult::Fail(e).to_string(), "fail");
    }

    #[test]
    fn spf_result_fail_display_with_explanation() {
        let e = ExplanationString::External("SPF failed".into());
        assert_eq!(&SpfResult::Fail(e).to_string(), "fail (SPF failed)");
    }
}
