// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

mod macros;
mod query;
mod record;
mod terms;

use crate::{
    config::Config,
    eval::query::{Query, QueryParams, QueryState, Resolver},
    lookup::Lookup,
    params::{DomainName, Sender},
    record::{ExplainString, Macro},
    result::{ErrorCause, ExpansionResult, QueryResult, SpfResult},
};
use async_trait::async_trait;
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    net::IpAddr,
};

#[async_trait]
trait Evaluate {
    async fn evaluate(&self, query: &mut Query<'_>, resolver: &Resolver<'_>) -> SpfResult;
}

#[async_trait]
trait EvaluateMatch {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult>;
}

enum MatchResult {
    Match,
    NoMatch,
}

#[async_trait]
trait EvaluateToString {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String>;
}

type EvalResult<T> = Result<T, EvalError>;

/// Errors that may occur during query evaluation.
#[derive(Debug)]
pub enum EvalError {
    /// The query timed out.
    Timeout,
    /// A DNS error occurred, with source attached if available. The error
    /// source may come from the `Lookup` implementation and is therefore of an
    /// arbitrary error type.
    Dns(Option<Box<dyn Error + Send + Sync>>),
    /// The lookup limit was exceeded.
    LookupLimitExceeded,
    /// The per-mechanism lookup limit was exceeded.
    PerMechanismLookupLimitExceeded,
    /// The void lookup limit was exceeded.
    VoidLookupLimitExceeded,
    /// A recursive query that failed with `Temperror` is being propagated.
    RecursiveTemperror,
    /// A recursive query that failed with `Permerror` is being propagated.
    RecursivePermerror,
    /// An *include* mechanism targeted a domain with no SPF record.
    IncludeNoRecord,
    /// A target domain name was in an invalid format.
    InvalidName(String),
    /// A domain specification contained an invalid macro.
    InvalidMacroInDomainSpec(Macro),
}

impl EvalError {
    fn to_spf_result(&self) -> SpfResult {
        match self {
            Self::Timeout | Self::Dns(_) | Self::RecursiveTemperror => SpfResult::Temperror,
            Self::LookupLimitExceeded
            | Self::PerMechanismLookupLimitExceeded
            | Self::VoidLookupLimitExceeded
            | Self::RecursivePermerror
            | Self::IncludeNoRecord
            // §4.8: ‘The result of evaluating check_host() with a syntactically
            // invalid domain is undefined. […] Some implementations choose to
            // treat such errors as not-match and therefore ignore such names,
            // while others return a "permerror" exception.’ This implementation
            // does in fact treat such errors as not-match, see the `Evaluate`
            // implementation in `crate::eval::record`. This variant maps to
            // `Permerror` specifically for use in `Redirect::evaluate`.
            | Self::InvalidName(_)
            | Self::InvalidMacroInDomainSpec(_) => SpfResult::Permerror,
        }
    }

    fn to_error_cause(&self) -> Option<ErrorCause> {
        use ErrorCause::*;
        match self {
            Self::Timeout => Some(Timeout),
            Self::Dns(_) => Some(Dns),
            Self::LookupLimitExceeded
            | Self::PerMechanismLookupLimitExceeded => Some(LookupLimitExceeded),
            Self::VoidLookupLimitExceeded => Some(VoidLookupLimitExceeded),
            Self::IncludeNoRecord => Some(NoSpfRecord),
            Self::InvalidName(_) | Self::InvalidMacroInDomainSpec(_) => Some(InvalidTargetDomain),
            // Recursive errors are not mapped.
            Self::RecursiveTemperror | Self::RecursivePermerror => None,
        }
    }
}

impl Error for EvalError {}

impl Display for EvalError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Timeout => write!(f, "query timed out"),
            Self::Dns(error) => {
                write!(f, "DNS error")?;
                if let Some(error) = error {
                    write!(f, ": {error}")?;
                }
                Ok(())
            }
            Self::LookupLimitExceeded => write!(f, "lookup limit exceeded"),
            Self::PerMechanismLookupLimitExceeded => write!(f, "per-mechanism lookup limit exceeded"),
            Self::VoidLookupLimitExceeded => write!(f, "void lookup limit exceeded"),
            Self::RecursiveTemperror => write!(f, "recursive query evaluated to \"temperror\" result"),
            Self::RecursivePermerror => write!(f, "recursive query evaluated to \"permerror\" result"),
            Self::IncludeNoRecord => write!(f, "no SPF record for include"),
            Self::InvalidName(name) => write!(f, "invalid target domain name \"{name}\""),
            Self::InvalidMacroInDomainSpec(macro_) => write!(f, "invalid macro \"{macro_}\" in domain spec"),
        }
    }
}

/// Performs an SPF query and evaluation for some sender identity.
///
/// This function corresponds to the [*check_host()*] function of RFC 7208.
/// The correspondence is not exact: The `ip` argument matches *&lt;ip&gt;*. The
/// `sender` argument covers both *&lt;sender&gt;* and *&lt;domain&gt;* (= the
/// domain part of *&lt;sender&gt;*). Finally, the `helo_domain` argument covers
/// an argument missing in *check_host()*, namely, the [`h` macro], which always
/// evaluates to the HELO/EHLO domain.
///
/// The argument `lookup` is an implementation of the [`Lookup`] trait, serving
/// as a DNS resolver. Provide an implementation yourself, or enable the Cargo
/// feature `trust-dns-resolver` to make an implementation available for
/// [`trust_dns_resolver::TokioAsyncResolver`].
///
/// When the feature `tokio-timeout` is enabled (enabled by default), the query
/// will abort after the timeout duration configured with
/// [`ConfigBuilder::timeout`][crate::ConfigBuilder::timeout].
///
/// # Examples
///
/// The following examples illustrate usage of this function for SPF
/// verification of a host.
///
/// ## Verifying the MAIL FROM identity
///
/// When verifying the MAIL FROM identity, the client host has presented some
/// email address with the `MAIL FROM` SMTP command, for example,
/// `amy@example.org`. Before that, it may also have presented some HELO/EHLO
/// hostname, for example, `mail.example.org`.
///
/// Pass these inputs as the `sender` and `helo_domain` argument, respectively.
///
/// In the example, notice how in accordance with [RFC 7208, section 4.3], an
/// unusable sender identity results in SPF result *none*.
///
/// ```
/// # use async_trait::async_trait;
/// # use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
/// # use tokio::runtime::Builder;
/// # use viaspf::{lookup::*, *};
/// #
/// # struct Resolver;
/// #
/// # #[async_trait]
/// # impl Lookup for Resolver {
/// #     async fn lookup_a(&self, _: &Name) -> LookupResult<Vec<Ipv4Addr>> { unimplemented!(); }
/// #     async fn lookup_aaaa(&self, _: &Name) -> LookupResult<Vec<Ipv6Addr>> { unimplemented!(); }
/// #     async fn lookup_mx(&self, _: &Name) -> LookupResult<Vec<Name>> { unimplemented!(); }
/// #     async fn lookup_txt(&self, _: &Name) -> LookupResult<Vec<String>> {
/// #         Ok(vec!["v=spf1 all".into()])
/// #     }
/// #     async fn lookup_ptr(&self, _: IpAddr) -> LookupResult<Vec<Name>> { unimplemented!(); }
/// # }
/// #
/// # Builder::new_current_thread().enable_all().build().unwrap().block_on(async {
/// # let resolver = Resolver;
/// let config = Default::default();
/// let ip = IpAddr::from([1, 2, 3, 4]);
/// let mail_from = "amy@example.org";
/// let helo = "mail.example.org";
///
/// let spf_result = match mail_from.parse() {
///     Ok(sender) => {
///         let helo_domain = helo.parse().ok();
///
///         evaluate_sender(&resolver, &config, ip, &sender, helo_domain.as_ref())
///             .await
///             .spf_result
///     }
///     _ => SpfResult::None,
/// };
///
/// assert_eq!(spf_result, SpfResult::Pass);
/// # });
/// ```
///
/// In the terms of the *check_host()* function specification,
/// * *&lt;ip&gt;* then is the IP address 1.2.3.4;
/// * *&lt;domain&gt;* is the domain part of the sender identity, that is
///   `example.org`;
/// * *&lt;sender&gt;* is the sender identity, that is `amy@example.org`.
///
/// ## Verifying the HELO identity
///
/// When verifying the HELO identity, the client host has presented some
/// hostname with the `HELO` or `EHLO` SMTP command, for example,
/// `mail.example.org`.
///
/// Pass this input as both the `sender` and `helo_domain` argument.
///
/// In the example, notice how in accordance with [RFC 7208, section 2.3], this
/// check is only performed when the HELO string is a valid domain name.
///
/// ```
/// # use async_trait::async_trait;
/// # use std::{error::Error, net::{IpAddr, Ipv4Addr, Ipv6Addr}};
/// # use tokio::runtime::Builder;
/// # use viaspf::{lookup::*, *};
/// #
/// # struct Resolver;
/// #
/// # #[async_trait]
/// # impl Lookup for Resolver {
/// #     async fn lookup_a(&self, _: &Name) -> LookupResult<Vec<Ipv4Addr>> { unimplemented!(); }
/// #     async fn lookup_aaaa(&self, _: &Name) -> LookupResult<Vec<Ipv6Addr>> { unimplemented!(); }
/// #     async fn lookup_mx(&self, _: &Name) -> LookupResult<Vec<Name>> { unimplemented!(); }
/// #     async fn lookup_txt(&self, _: &Name) -> LookupResult<Vec<String>> {
/// #         Ok(vec!["v=spf1 all".into()])
/// #     }
/// #     async fn lookup_ptr(&self, _: IpAddr) -> LookupResult<Vec<Name>> { unimplemented!(); }
/// # }
/// #
/// # Builder::new_current_thread().enable_all().build().unwrap().block_on(async {
/// # let resolver = Resolver;
/// let config = Default::default();
/// let ip = IpAddr::from([1, 2, 3, 4]);
/// let helo = "mail.example.org";
///
/// let sender = Sender::from_domain(helo)?;
/// let helo_domain = sender.domain();
///
/// let result = evaluate_sender(&resolver, &config, ip, &sender, Some(helo_domain)).await;
///
/// assert_eq!(result.spf_result, SpfResult::Pass);
/// # Ok::<_, Box<dyn Error>>(())
/// # });
/// ```
///
/// In the terms of the *check_host()* function specification,
/// * *&lt;ip&gt;* then is the IP address 1.2.3.4;
/// * *&lt;domain&gt;* is the sender identity, that is `mail.example.org`;
/// * *&lt;sender&gt;* itself gets qualified with the local part `postmaster`
///   (see [section 4.3][RFC 7208, section 4.3] in the RFC) to become
///   `postmaster@mail.example.org`.
///
/// [*check_host()*]: https://www.rfc-editor.org/rfc/rfc7208#section-4
/// [`h` macro]: crate::record::MacroKind::HeloDomain
/// [`trust_dns_resolver::TokioAsyncResolver`]: https://docs.rs/trust-dns-resolver/0.22.0/trust_dns_resolver/type.TokioAsyncResolver.html
/// [RFC 7208, section 4.3]: https://www.rfc-editor.org/rfc/rfc7208#section-4.3
/// [RFC 7208, section 2.3]: https://www.rfc-editor.org/rfc/rfc7208#section-2.3
pub async fn evaluate_sender(
    lookup: &impl Lookup,
    config: &Config,
    ip: IpAddr,
    sender: &Sender,
    helo_domain: Option<&DomainName>,
) -> QueryResult {
    let sender = sender.clone();
    let domain = sender.domain().clone().into_name();
    let helo_domain = domain_to_string(helo_domain);
    let ip = normalize_ip(ip);

    let mut query = Query::new(
        config,
        QueryParams::new(ip, domain, sender, helo_domain),
        QueryState::new(),
    );

    let resolver = Resolver::new(lookup);

    let f = query.execute(&resolver);

    #[cfg(feature = "tokio-timeout")]
    let f = tokio::time::timeout(config.timeout(), f);

    let spf_result = f.await;

    #[cfg(feature = "tokio-timeout")]
    let spf_result = match spf_result {
        Ok(r) => r,
        Err(_) => {
            query.result_cause = Some(ErrorCause::Timeout.into());
            SpfResult::Temperror
        }
    };

    let cause = query.result_cause;

    let trace = if config.capture_trace() {
        Some(query.trace)
    } else {
        None
    };

    QueryResult {
        spf_result,
        cause,
        trace,
    }
}

/// Performs macro expansion on an explain string.
///
/// An *explain string* is a kind of macro string found in SPF explanations, see
/// RFC 7208, [sections 6.2][RFC 7208, section 6.2] and [7][RFC 7208, section
/// 7].
///
/// Note that the [`p` macro], the validated domain name of the IP address, is
/// the only macro that will query the DNS using the `Lookup` implementation.
/// When including this macro in the explain string be aware that expansion may
/// not be immediate and may be delayed up to the timeout configured.
///
/// All other macros are expanded locally without using `lookup`, and the
/// expansion is returned immediately.
///
/// When the feature `tokio-timeout` is enabled (enabled by default), the query
/// will abort after the timeout duration configured with
/// [`ConfigBuilder::timeout`][crate::ConfigBuilder::timeout].
///
/// # Examples
///
/// ```
/// # use async_trait::async_trait;
/// # use std::{error::Error, net::{IpAddr, Ipv4Addr, Ipv6Addr}};
/// # use tokio::runtime::Builder;
/// # use viaspf::{lookup::*, *};
/// #
/// # struct Resolver;
/// #
/// # #[async_trait]
/// # impl Lookup for Resolver {
/// #     async fn lookup_a(&self, _: &Name) -> LookupResult<Vec<Ipv4Addr>> {
/// #         Ok(vec![[1, 2, 3, 4].into()])
/// #     }
/// #     async fn lookup_aaaa(&self, _: &Name) -> LookupResult<Vec<Ipv6Addr>> { unimplemented!(); }
/// #     async fn lookup_mx(&self, _: &Name) -> LookupResult<Vec<Name>> { unimplemented!(); }
/// #     async fn lookup_txt(&self, _: &Name) -> LookupResult<Vec<String>> { unimplemented!(); }
/// #     async fn lookup_ptr(&self, _: IpAddr) -> LookupResult<Vec<Name>> {
/// #         Ok(vec![Name::new("mail.example.org").unwrap()])
/// #     }
/// # }
/// #
/// # Builder::new_current_thread().enable_all().build().unwrap().block_on(async {
/// # let resolver = Resolver;
/// let config = Default::default();
/// let explain_string = "l=%{l} o=%{o} ip=%{c} ptr=%{p}".parse()?;
/// let ip = IpAddr::from([1, 2, 3, 4]);
/// let sender = "amy@example.org".parse()?;
///
/// let result =
///     expand_explain_string(&resolver, &config, &explain_string, ip, &sender, None).await;
///
/// assert_eq!(
///     result.expansion,
///     "l=amy o=example.org ip=1.2.3.4 ptr=mail.example.org"
/// );
/// # Ok::<_, Box<dyn Error>>(())
/// # });
/// ```
///
/// [RFC 7208, section 6.2]: https://www.rfc-editor.org/rfc/rfc7208#section-6.2
/// [RFC 7208, section 7]: https://www.rfc-editor.org/rfc/rfc7208#section-7
/// [`p` macro]: crate::record::MacroKind::ValidatedDomain
pub async fn expand_explain_string(
    lookup: &impl Lookup,
    config: &Config,
    explain_string: &ExplainString,
    ip: IpAddr,
    sender: &Sender,
    helo_domain: Option<&DomainName>,
) -> ExpansionResult {
    let sender = sender.clone();
    let domain = sender.domain().clone().into_name();
    let helo_domain = domain_to_string(helo_domain);
    let ip = normalize_ip(ip);

    let mut state = QueryState::new();
    state.reset_for_explain_string();

    let mut query = Query::new(
        config,
        QueryParams::new(ip, domain, sender, helo_domain),
        state,
    );

    let resolver = Resolver::new(lookup);

    let f = explain_string.evaluate_to_string(&mut query, &resolver);

    #[cfg(feature = "tokio-timeout")]
    let f = tokio::time::timeout(config.timeout(), f);

    let eval_result = f.await;

    #[cfg(feature = "tokio-timeout")]
    let eval_result = match eval_result {
        Ok(r) => r,
        Err(_) => Err(EvalError::Timeout),
    };

    let expansion = match eval_result {
        Ok(expansion) => expansion,
        Err(_) => {
            // On failure (and on timeout), the expansion is the evaluated
            // explain string, but with all `p` macros replaced with
            // `"unknown"`. Note that this may leave the trace in a state where
            // it contains both evaluation attempts, but this is acceptable.
            macros::safely_expand_explain_string(&mut query, &resolver, explain_string).await
        }
    };

    let trace = if config.capture_trace() {
        Some(query.trace)
    } else {
        None
    };

    ExpansionResult { expansion, trace }
}

// In a dual-stack network, an IPv4 address can be mapped as an IPv4-mapped IPv6
// address. See §5; the wording in RFC 4408 was even clearer: ‘Even if the SMTP
// connection is via IPv6, an IPv4-mapped IPv6 IP address […] MUST still be
// considered an IPv4 address.’
#[inline]
fn normalize_ip(ip: IpAddr) -> IpAddr {
    match ip {
        IpAddr::V4(_) => ip,
        IpAddr::V6(ip6) => match ip6.octets() {
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xff, 0xff, a, b, c, d] => IpAddr::from([a, b, c, d]),
            _ => ip,
        },
    }
}

// The specification does not elaborate on what should happen when expanding an
// `h` macro with an invalid HELO domain. Fall back to ‘unknown’.
fn domain_to_string(helo_domain: Option<&DomainName>) -> String {
    helo_domain.map_or_else(|| "unknown".into(), |h| h.to_string())
}
