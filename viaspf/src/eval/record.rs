// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use crate::{
    eval::{
        query::{Query, Resolver},
        EvalError, Evaluate, EvaluateMatch, EvaluateToString, MatchResult,
    },
    record::{Explanation, Modifier, Qualifier, Record, Redirect},
    result::{ErrorCause, ExplanationString, SpfResult},
    trace::Tracepoint,
};
use async_trait::async_trait;

#[async_trait]
impl Evaluate for Record {
    async fn evaluate(&self, query: &mut Query<'_>, resolver: &Resolver<'_>) -> SpfResult {
        trace!(query, Tracepoint::EvaluateRecord(self.clone()));

        // §6: ‘These two modifiers MUST NOT appear in a record more than once
        // each. If they do, then check_host() exits with a result of
        // "permerror".’
        let redirect = match get_redirect(self) {
            Ok(r) => r,
            Err(redirects) => {
                trace!(query, Tracepoint::MultipleRedirectModifiers(redirects));
                query.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };
        let explanation = match get_explanation(self) {
            Ok(e) => e,
            Err(explanations) => {
                trace!(query, Tracepoint::MultipleExpModifiers(explanations));
                query.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };

        // §4.6.2: ‘Each mechanism is considered in turn from left to right.’
        for directive in self.directives() {
            trace!(query, Tracepoint::EvaluateDirective(directive.clone()));

            let mechanism = &directive.mechanism;

            // ‘When a mechanism is evaluated, one of three things can happen:
            // it can match, not match, or return an exception.’
            match mechanism.evaluate_match(query, resolver).await {
                // ‘If it matches, processing ends and the qualifier value is
                // returned as the result of that record.’
                Ok(MatchResult::Match) => {
                    trace!(query, Tracepoint::MechanismMatch);

                    query.result_cause = Some(mechanism.clone().into());

                    let spf_result = match directive.qualifier.unwrap_or_default() {
                        Qualifier::Pass => SpfResult::Pass,
                        Qualifier::Fail => {
                            let e = compute_explanation(explanation, query, resolver).await;
                            SpfResult::Fail(e)
                        }
                        Qualifier::Neutral => SpfResult::Neutral,
                        Qualifier::Softfail => SpfResult::Softfail,
                    };

                    trace!(query, Tracepoint::DirectiveResult(spf_result.clone()));
                    return spf_result;
                }

                // ‘If it does not match, processing continues with the next
                // mechanism.’ In addition, invalid domain name expansions are
                // treated as not-match in accordance with §4.8.
                Ok(MatchResult::NoMatch) | Err(EvalError::InvalidName(_)) => {
                    trace!(query, Tracepoint::MechanismNoMatch);

                    // Any result cause from a recursive match is superseded and
                    // needs to be cleared.
                    query.result_cause = None;

                    continue;
                }

                // ‘If it returns an exception, mechanism processing ends and
                // the exception value is returned.’
                Err(e) => {
                    // No store when returning from recursive evaluations.
                    if let Some(e) = e.to_error_cause() {
                        query.result_cause = Some(e.into());
                    }

                    let spf_result = e.to_spf_result();
                    trace!(query, Tracepoint::MechanismErrorResult(e, spf_result.clone()));
                    return spf_result;
                }
            }
        }

        // §6.1: ‘If all mechanisms fail to match, and a "redirect" modifier is
        // present’ …
        if let Some(redirect) = redirect {
            let spf_result = redirect.evaluate(query, resolver).await;
            trace!(query, Tracepoint::RedirectResult(spf_result.clone()));
            return spf_result;
        }

        // §4.7: ‘If none of the mechanisms match and there is no "redirect"
        // modifier, then the check_host() returns a result of "neutral"’.
        trace!(query, Tracepoint::NeutralResult);
        SpfResult::Neutral
    }
}

fn get_redirect(record: &Record) -> Result<Option<&Redirect>, Vec<Redirect>> {
    let mut redirects = record.modifiers().filter_map(|modifier| match modifier {
        Modifier::Redirect(r) => Some(r),
        _ => None,
    });

    match redirects.next() {
        None => Ok(None),
        Some(redirect) => {
            let mut rest = redirects.cloned().collect::<Vec<_>>();
            match *rest {
                [] => Ok(Some(redirect)),
                [..] => {
                    rest.insert(0, redirect.clone());
                    Err(rest)
                }
            }
        }
    }
}

fn get_explanation(record: &Record) -> Result<Option<&Explanation>, Vec<Explanation>> {
    let mut explanations = record.modifiers().filter_map(|modifier| match modifier {
        Modifier::Explanation(e) => Some(e),
        _ => None,
    });

    match explanations.next() {
        None => Ok(None),
        Some(explanation) => {
            let mut rest = explanations.cloned().collect::<Vec<_>>();
            match *rest {
                [] => Ok(Some(explanation)),
                [..] => {
                    rest.insert(0, explanation.clone());
                    Err(rest)
                }
            }
        }
    }
}

async fn compute_explanation(
    explanation: Option<&Explanation>,
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> ExplanationString {
    // §6.2: ‘During recursion into an "include" mechanism, an "exp" modifier
    // from the <target-name> MUST NOT be used.’
    if query.state.is_included_query() {
        return Default::default();  // placeholder value, to be discarded
    }

    // §4.6.4: ‘the "exp" modifier -- do[es] not cause DNS queries at the time
    // of SPF evaluation (the "exp" modifier only causes a lookup at a later
    // time), and [its] use is not subject to this limit.’ As computing the
    // explanation is the very last thing done in a query, the lookup counts are
    // simply unconditionally reset here. Limits remain in effect but are never
    // exceeded in normal circumstances.
    query.state.reset_for_explain_string();

    match explanation {
        None => {
            trace!(query, Tracepoint::DefaultExplanation);
            ExplanationString::Default
        }
        Some(explanation) => match explanation.evaluate_to_string(query, resolver).await {
            Ok(explanation) => {
                let explanation_string = ExplanationString::External(explanation);
                trace!(query, Tracepoint::ExplanationStringResult(explanation_string.clone()));
                explanation_string
            }
            Err(e) => {
                trace!(query, Tracepoint::ExplanationStringErrorResult(e));
                ExplanationString::Default
            }
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_sole_redirect_ok() {
        let record = "v=spf1 redirect=one.example.org redirect=two.example.org"
            .parse()
            .unwrap();
        assert!(get_redirect(&record).is_err());
    }
}
