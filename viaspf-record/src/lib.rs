// viaspf – implementation of the SPF specification
// Copyright © 2020–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

//! A library containing a data model and parser for *Sender Policy Framework*
//! (SPF) records.
//!
//! The data structures in this library are modeled after the ABNF in [RFC 7208,
//! section 12].
//!
//! # Usage
//!
//! The main top-level data structure in this module is the [`Record`] struct.
//! `Record` represents a syntactically valid SPF record. A `Record` can be
//! constructed programmatically or parsed from a string:
//!
//! ```
//! use std::net::Ipv4Addr;
//! use viaspf_record::*;
//!
//! let record = "v=spf1 mx ip4:12.34.56.78/24 -all".parse();
//!
//! assert_eq!(
//!     record,
//!     Ok(Record {
//!         terms: vec![
//!             Term::Directive(Directive {
//!                 qualifier: None,
//!                 mechanism: Mechanism::Mx(Mx {
//!                     domain_spec: None,
//!                     prefix_len: None,
//!                 }),
//!             }),
//!             Term::Directive(Directive {
//!                 qualifier: None,
//!                 mechanism: Mechanism::Ip4(Ip4 {
//!                     addr: Ipv4Addr::new(12, 34, 56, 78),
//!                     prefix_len: Some(Ip4CidrLength::new(24).unwrap()),
//!                 }),
//!             }),
//!             Term::Directive(Directive {
//!                 qualifier: Some(Qualifier::Fail),
//!                 mechanism: Mechanism::All,
//!             }),
//!         ],
//!     })
//! );
//! ```
//!
//! Also provided as a top-level data structure is the [`ExplainString`] struct.
//! It can be parsed from a string in the same way.
//!
//! SPF record data may be printed via `Display`. The string representations are
//! the canonical SPF string representations. Thus roundtripping through parsing
//! and printing is possible:
//!
//! ```
//! use viaspf_record::Record;
//!
//! let record_str = "v=spf1 mx -all";
//! assert_eq!(record_str.parse::<Record>()?.to_string(), record_str);
//! # Ok::<_, viaspf_record::ParseError>(())
//! ```
//!
//! [RFC 7208, section 12]: https://www.rfc-editor.org/rfc/rfc7208#section-12

mod parse;

use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    iter::FromIterator,
    net::{Ipv4Addr, Ipv6Addr},
    num::NonZeroU8,
    vec::IntoIter,
};

/// An error indicating that SPF record data could not be parsed.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ParseError;

impl Error for ParseError {}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to parse SPF record data")
    }
}

/// An SPF record.
///
/// # Examples
///
/// ```
/// use viaspf_record::*;
///
/// let record = "v=spf1 -all".parse::<Record>()?;
///
/// assert_eq!(
///     record,
///     Record {
///         terms: vec![
///             Term::Directive(Directive {
///                 qualifier: Some(Qualifier::Fail),
///                 mechanism: Mechanism::All,
///             }),
///         ],
///     }
/// );
///
/// assert_eq!(record.to_string(), "v=spf1 -all");
/// # Ok::<_, ParseError>(())
/// ```
#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct Record {
    /// The record’s terms.
    pub terms: Vec<Term>,
}

impl Record {
    /// Returns an iterator over the record’s directives.
    pub fn directives(&self) -> impl Iterator<Item = &Directive> {
        self.terms.iter().filter_map(|term| match term {
            Term::Directive(d) => Some(d),
            _ => None,
        })
    }

    /// Returns an iterator over the record’s modifiers.
    pub fn modifiers(&self) -> impl Iterator<Item = &Modifier> {
        self.terms.iter().filter_map(|term| match term {
            Term::Modifier(m) => Some(m),
            _ => None,
        })
    }
}

impl IntoIterator for Record {
    type Item = Term;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.terms.into_iter()
    }
}

impl Display for Record {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "v=spf1")?;
        for term in &self.terms {
            write!(f, " {}", term)?;
        }
        Ok(())
    }
}

impl From<Vec<Term>> for Record {
    fn from(terms: Vec<Term>) -> Self {
        Self { terms }
    }
}

impl FromIterator<Term> for Record {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = Term>,
    {
        Self {
            terms: iter.into_iter().collect(),
        }
    }
}

/// A term.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Term {
    /// The directive term.
    Directive(Directive),
    /// The modifier term.
    Modifier(Modifier),
}

impl Display for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Directive(directive) => directive.fmt(f),
            Self::Modifier(modifier) => modifier.fmt(f),
        }
    }
}

impl From<Directive> for Term {
    fn from(directive: Directive) -> Self {
        Self::Directive(directive)
    }
}

impl From<Modifier> for Term {
    fn from(modifier: Modifier) -> Self {
        Self::Modifier(modifier)
    }
}

/// A directive.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Directive {
    /// The directive’s qualifier.
    pub qualifier: Option<Qualifier>,
    /// The directive’s mechanism.
    pub mechanism: Mechanism,
}

impl Display for Directive {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Some(qualifier) = self.qualifier {
            qualifier.fmt(f)?;
        }
        self.mechanism.fmt(f)
    }
}

/// A qualifier.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Qualifier {
    /// The *+* qualifier.
    Pass,
    /// The *-* qualifier.
    Fail,
    /// The *?* qualifier.
    Neutral,
    /// The *~* qualifier.
    Softfail,
}

impl Default for Qualifier {
    fn default() -> Self {
        Self::Pass
    }
}

impl Display for Qualifier {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Pass => write!(f, "+"),
            Self::Fail => write!(f, "-"),
            Self::Neutral => write!(f, "?"),
            Self::Softfail => write!(f, "~"),
        }
    }
}

/// A mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Mechanism {
    /// The *all* mechanism.
    All,
    /// The *include* mechanism.
    Include(Include),
    /// The *a* mechanism.
    A(A),
    /// The *mx* mechanism.
    Mx(Mx),
    /// The *ptr* mechanism.
    Ptr(Ptr),
    /// The *ip4* mechanism.
    Ip4(Ip4),
    /// The *ip6* mechanism.
    Ip6(Ip6),
    /// The *exists* mechanism.
    Exists(Exists),
}

impl Display for Mechanism {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::All => write!(f, "all"),
            Self::Include(include) => include.fmt(f),
            Self::A(a) => a.fmt(f),
            Self::Mx(mx) => mx.fmt(f),
            Self::Ptr(ptr) => ptr.fmt(f),
            Self::Ip4(ip4) => ip4.fmt(f),
            Self::Ip6(ip6) => ip6.fmt(f),
            Self::Exists(exists) => exists.fmt(f),
        }
    }
}

impl From<Include> for Mechanism {
    fn from(include: Include) -> Self {
        Self::Include(include)
    }
}

impl From<A> for Mechanism {
    fn from(a: A) -> Self {
        Self::A(a)
    }
}

impl From<Mx> for Mechanism {
    fn from(mx: Mx) -> Self {
        Self::Mx(mx)
    }
}

impl From<Ptr> for Mechanism {
    fn from(ptr: Ptr) -> Self {
        Self::Ptr(ptr)
    }
}

impl From<Ip4> for Mechanism {
    fn from(ip4: Ip4) -> Self {
        Self::Ip4(ip4)
    }
}

impl From<Ip6> for Mechanism {
    fn from(ip6: Ip6) -> Self {
        Self::Ip6(ip6)
    }
}

impl From<Exists> for Mechanism {
    fn from(exists: Exists) -> Self {
        Self::Exists(exists)
    }
}

/// An *include* mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Include {
    /// The mechanism’s domain specification.
    pub domain_spec: DomainSpec,
}

impl Display for Include {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "include:{}", self.domain_spec)
    }
}

/// An *a* mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct A {
    /// The mechanism’s domain specification.
    pub domain_spec: Option<DomainSpec>,
    /// The mechanism’s CIDR prefix length.
    pub prefix_len: Option<DualCidrLength>,
}

impl Display for A {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "a")?;
        if let Some(domain_spec) = &self.domain_spec {
            write!(f, ":{}", domain_spec)?;
        }
        if let Some(len) = self.prefix_len {
            fmt_dual_cidr_length(f, len)?;
        }
        Ok(())
    }
}

/// An *mx* mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Mx {
    /// The mechanism’s domain specification.
    pub domain_spec: Option<DomainSpec>,
    /// The mechanism’s CIDR prefix length.
    pub prefix_len: Option<DualCidrLength>,
}

impl Display for Mx {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "mx")?;
        if let Some(domain_spec) = &self.domain_spec {
            write!(f, ":{}", domain_spec)?;
        }
        if let Some(len) = self.prefix_len {
            fmt_dual_cidr_length(f, len)?;
        }
        Ok(())
    }
}

fn fmt_dual_cidr_length(f: &mut Formatter<'_>, prefix_len: DualCidrLength) -> fmt::Result {
    match prefix_len {
        DualCidrLength::Ip4(len) => write!(f, "/{}", len),
        DualCidrLength::Ip6(len) => write!(f, "//{}", len),
        DualCidrLength::Both(len4, len6) => write!(f, "/{}//{}", len4, len6),
    }
}

/// A *ptr* mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Ptr {
    /// The mechanism’s domain specification.
    pub domain_spec: Option<DomainSpec>,
}

impl Display for Ptr {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "ptr")?;
        if let Some(domain_spec) = &self.domain_spec {
            write!(f, ":{}", domain_spec)?;
        }
        Ok(())
    }
}

/// An *ip4* mechanism.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Ip4 {
    /// The mechanism’s network address.
    pub addr: Ipv4Addr,
    /// The mechanism’s CIDR prefix length.
    pub prefix_len: Option<Ip4CidrLength>,
}

impl Display for Ip4 {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "ip4:{}", self.addr)?;
        if let Some(len) = self.prefix_len {
            write!(f, "/{}", len)?;
        }
        Ok(())
    }
}

/// An *ip6* mechanism.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Ip6 {
    /// The mechanism’s network address.
    pub addr: Ipv6Addr,
    /// The mechanism’s CIDR prefix length.
    pub prefix_len: Option<Ip6CidrLength>,
}

impl Display for Ip6 {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "ip6:{}", self.addr)?;
        if let Some(len) = self.prefix_len {
            write!(f, "/{}", len)?;
        }
        Ok(())
    }
}

/// An *exists* mechanism.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Exists {
    /// The mechanism’s domain specification.
    pub domain_spec: DomainSpec,
}

impl Display for Exists {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "exists:{}", self.domain_spec)
    }
}

/// A modifier.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Modifier {
    /// The *redirect* modifier.
    Redirect(Redirect),
    /// The *exp* modifier.
    Explanation(Explanation),
    /// The unknown modifier.
    Unknown(Unknown),
}

impl Display for Modifier {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Redirect(redirect) => redirect.fmt(f),
            Self::Explanation(explanation) => explanation.fmt(f),
            Self::Unknown(unknown) => unknown.fmt(f),
        }
    }
}

impl From<Redirect> for Modifier {
    fn from(redirect: Redirect) -> Self {
        Self::Redirect(redirect)
    }
}

impl From<Explanation> for Modifier {
    fn from(explanation: Explanation) -> Self {
        Self::Explanation(explanation)
    }
}

impl From<Unknown> for Modifier {
    fn from(unknown: Unknown) -> Self {
        Self::Unknown(unknown)
    }
}

/// A *redirect* modifier.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Redirect {
    /// The modifier’s domain specification.
    pub domain_spec: DomainSpec,
}

impl Display for Redirect {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "redirect={}", self.domain_spec)
    }
}

/// An *exp* modifier.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Explanation {
    /// The modifier’s domain specification.
    pub domain_spec: DomainSpec,
}

impl Display for Explanation {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "exp={}", self.domain_spec)
    }
}

/// An unknown modifier.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Unknown {
    name: Name,
    value: MacroString,
}

impl Unknown {
    /// Creates an unknown modifier with the given name and value.
    ///
    /// # Errors
    ///
    /// If `name` is not a valid unknown modifier name, it is returned as the
    /// `Err` variant.
    pub fn new<N, V>(name: N, value: V) -> Result<Self, Name>
    where
        N: Into<Name>,
        V: Into<MacroString>,
    {
        let name = name.into();
        if is_unknown_modifier_name(&name) {
            Ok(Self {
                name,
                value: value.into(),
            })
        } else {
            Err(name)
        }
    }

    /// Returns the modifier’s name.
    pub fn name(&self) -> &Name {
        &self.name
    }

    /// Returns the modifier’s value.
    pub fn value(&self) -> &MacroString {
        &self.value
    }
}

fn is_unknown_modifier_name(name: &Name) -> bool {
    !name.0.eq_ignore_ascii_case("redirect") && !name.0.eq_ignore_ascii_case("exp")
}

impl Display for Unknown {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}={}", self.name, self.value)
    }
}

/// A modifier name.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Name(String);

impl Name {
    /// Creates a modifier name with the given name.
    ///
    /// # Errors
    ///
    /// If the given argument is invalid, it is returned as the `Err` variant.
    pub fn new<S>(s: S) -> Result<Self, String>
    where
        S: Into<String>,
    {
        let s = s.into();
        if is_name_str(&s) {
            Ok(Self(s))
        } else {
            Err(s)
        }
    }
}

fn is_name_str(s: &str) -> bool {
    s.starts_with(is_alpha) && s.chars().all(is_name_char)
}

fn is_name_char(c: char) -> bool {
    is_alphanum(c) || matches!(c, '-' | '_' | '.')
}

impl Display for Name {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl AsRef<str> for Name {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<Name> for String {
    fn from(name: Name) -> Self {
        name.0
    }
}

/// A combined IPv4 and/or IPv6 CIDR prefix length.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum DualCidrLength {
    /// The IPv4 CIDR prefix length.
    Ip4(Ip4CidrLength),
    /// The IPv6 CIDR prefix length.
    Ip6(Ip6CidrLength),
    /// Both the IPv4 and IPv6 CIDR prefix lengths.
    Both(Ip4CidrLength, Ip6CidrLength),
}

impl DualCidrLength {
    /// Returns the IPv4 CIDR prefix length, if present.
    pub fn ip4(self) -> Option<Ip4CidrLength> {
        match self {
            Self::Ip4(len) | Self::Both(len, _) => Some(len),
            Self::Ip6(_) => None,
        }
    }

    /// Returns the IPv6 CIDR prefix length, if present.
    pub fn ip6(self) -> Option<Ip6CidrLength> {
        match self {
            Self::Ip6(len) | Self::Both(_, len) => Some(len),
            Self::Ip4(_) => None,
        }
    }
}

impl Default for DualCidrLength {
    fn default() -> Self {
        Self::Both(Default::default(), Default::default())
    }
}

impl From<Ip4CidrLength> for DualCidrLength {
    fn from(len: Ip4CidrLength) -> Self {
        Self::Ip4(len)
    }
}

impl From<Ip6CidrLength> for DualCidrLength {
    fn from(len: Ip6CidrLength) -> Self {
        Self::Ip6(len)
    }
}

/// An IPv4 CIDR prefix length.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Ip4CidrLength(u8);

impl Ip4CidrLength {
    /// Creates an IPv4 CIDR prefix length with the given value, if it is within
    /// bounds.
    pub fn new(len: u8) -> Option<Self> {
        if is_ip4_cidr_length(&len) {
            Some(Self(len))
        } else {
            None
        }
    }

    /// Returns the CIDR prefix length’s integer value.
    pub fn get(self) -> u8 {
        self.0
    }
}

fn is_ip4_cidr_length(len: &u8) -> bool {
    matches!(len, 0..=32)
}

impl Default for Ip4CidrLength {
    fn default() -> Self {
        Self(32)
    }
}

impl Display for Ip4CidrLength {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<Ip4CidrLength> for u8 {
    fn from(len: Ip4CidrLength) -> Self {
        len.0
    }
}

/// An IPv6 CIDR prefix length.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Ip6CidrLength(u8);

impl Ip6CidrLength {
    /// Creates an IPv6 CIDR prefix length with the given value, if it is within
    /// bounds.
    pub fn new(len: u8) -> Option<Self> {
        if is_ip6_cidr_length(&len) {
            Some(Self(len))
        } else {
            None
        }
    }

    /// Returns the CIDR prefix length’s integer value.
    pub fn get(self) -> u8 {
        self.0
    }
}

fn is_ip6_cidr_length(len: &u8) -> bool {
    matches!(len, 0..=128)
}

impl Default for Ip6CidrLength {
    fn default() -> Self {
        Self(128)
    }
}

impl Display for Ip6CidrLength {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<Ip6CidrLength> for u8 {
    fn from(len: Ip6CidrLength) -> Self {
        len.0
    }
}

// In this ‘domain-spec’ model there is no counterpart to ‘domain-end’ from the
// RFC. Instead parsing and argument validation ensure that the given macro
// string is in an acceptable format.
/// A domain specification.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct DomainSpec(MacroString);

impl DomainSpec {
    /// Creates a domain specification with the given macro string.
    ///
    /// # Errors
    ///
    /// If the given argument is invalid, it is returned as the `Err` variant.
    pub fn new<M>(macro_string: M) -> Result<Self, MacroString>
    where
        M: Into<MacroString>,
    {
        let macro_string = macro_string.into();
        if has_valid_domain_end(&macro_string) {
            Ok(Self(macro_string))
        } else {
            Err(macro_string)
        }
    }
}

fn has_valid_domain_end(macro_string: &MacroString) -> bool {
    match &macro_string.segments[..] {
        [] => false,
        [.., MacroStringSegment::MacroExpand(_)] => true,
        literals @ [.., MacroStringSegment::MacroLiteral(_)] => {
            // Join multiple final `MacroLiteral` segments into one before
            // checking for a valid ‘domain-end’.
            let i = literals
                .iter()
                .rposition(|s| !matches!(s, MacroStringSegment::MacroLiteral(_)))
                .map_or(0, |i| i + 1);
            let mut s = literals[i..]
                .iter()
                .map(|s| match s {
                    MacroStringSegment::MacroLiteral(l) => l.as_ref(),
                    _ => unreachable!(),
                })
                .collect::<String>();

            if s.ends_with('.') {
                s.pop();
            }

            matches!(s.rsplit_once('.'), Some((_, s)) if is_toplabel(s))
        }
    }
}

// §7.1: ‘The "toplabel" construction is subject to the letter-digit-hyphen
// (LDH) rule plus additional top-level domain (TLD) restrictions.’ (Note: This
// is similar to code in `viaspf::lookup`.)
fn is_toplabel(s: &str) -> bool {
    matches!(s.len(), 1..=63)
        && s.starts_with(|c: char| c.is_ascii_alphanumeric())
        && s.ends_with(|c: char| c.is_ascii_alphanumeric())
        && s.chars().all(|c: char| c.is_ascii_alphanumeric() || c == '-')
        && !s.chars().all(|c: char| c.is_ascii_digit())
}

impl Display for DomainSpec {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl AsRef<MacroString> for DomainSpec {
    fn as_ref(&self) -> &MacroString {
        &self.0
    }
}

impl From<DomainSpec> for MacroString {
    fn from(domain_spec: DomainSpec) -> Self {
        domain_spec.0
    }
}

/// An explain string.
///
/// # Examples
///
/// ```
/// use viaspf_record::ExplainString;
///
/// let explain_string = "Host %{c} not authorized".parse::<ExplainString>()?;
///
/// assert_eq!(explain_string.to_string(), "Host %{c} not authorized");
/// # Ok::<_, viaspf_record::ParseError>(())
/// ```
#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct ExplainString {
    /// The explain string’s parts.
    pub segments: Vec<ExplainStringSegment>,
}

impl IntoIterator for ExplainString {
    type Item = ExplainStringSegment;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.segments.into_iter()
    }
}

impl Display for ExplainString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for segment in &self.segments {
            segment.fmt(f)?;
        }
        Ok(())
    }
}

impl From<Vec<ExplainStringSegment>> for ExplainString {
    fn from(segments: Vec<ExplainStringSegment>) -> Self {
        Self { segments }
    }
}

impl FromIterator<ExplainStringSegment> for ExplainString {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = ExplainStringSegment>,
    {
        Self {
            segments: iter.into_iter().collect(),
        }
    }
}

/// A part of an explain string.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ExplainStringSegment {
    /// The macro string segment.
    MacroString(MacroString),
    /// The space segment.
    Space,
}

impl Display for ExplainStringSegment {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::MacroString(macro_string) => macro_string.fmt(f),
            Self::Space => write!(f, " "),
        }
    }
}

impl From<MacroString> for ExplainStringSegment {
    fn from(macro_string: MacroString) -> Self {
        Self::MacroString(macro_string)
    }
}

/// A macro string.
#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct MacroString {
    /// The macro string’s parts.
    pub segments: Vec<MacroStringSegment>,
}

impl IntoIterator for MacroString {
    type Item = MacroStringSegment;
    type IntoIter = IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.segments.into_iter()
    }
}

impl Display for MacroString {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for segment in &self.segments {
            segment.fmt(f)?;
        }
        Ok(())
    }
}

impl From<Vec<MacroStringSegment>> for MacroString {
    fn from(segments: Vec<MacroStringSegment>) -> Self {
        Self { segments }
    }
}

impl FromIterator<MacroStringSegment> for MacroString {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = MacroStringSegment>,
    {
        Self {
            segments: iter.into_iter().collect(),
        }
    }
}

/// A part of a macro string.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum MacroStringSegment {
    /// The macro-expand segment.
    MacroExpand(MacroExpand),
    /// The literal segment.
    MacroLiteral(MacroLiteral),
}

impl Display for MacroStringSegment {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::MacroExpand(macro_expand) => macro_expand.fmt(f),
            Self::MacroLiteral(macro_literal) => macro_literal.fmt(f),
        }
    }
}

impl From<MacroExpand> for MacroStringSegment {
    fn from(macro_expand: MacroExpand) -> Self {
        Self::MacroExpand(macro_expand)
    }
}

impl From<MacroLiteral> for MacroStringSegment {
    fn from(macro_literal: MacroLiteral) -> Self {
        Self::MacroLiteral(macro_literal)
    }
}

/// A part of a macro string that may be expanded.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum MacroExpand {
    /// The macro expansion.
    Macro(Macro),
    /// The escape sequence expansion.
    Escape(Escape),
}

impl Display for MacroExpand {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Macro(macro_) => macro_.fmt(f),
            Self::Escape(escape) => escape.fmt(f),
        }
    }
}

impl From<Macro> for MacroExpand {
    fn from(macro_: Macro) -> Self {
        Self::Macro(macro_)
    }
}

impl From<Escape> for MacroExpand {
    fn from(escape: Escape) -> Self {
        Self::Escape(escape)
    }
}

/// A macro.
///
/// # Examples
///
/// ```
/// use std::num::NonZeroU8;
/// use viaspf_record::*;
///
/// let mut m = Macro::new(MacroKind::Domain);
/// assert_eq!(m.to_string(), "%{d}");
///
/// m.set_url_encode(true);
/// m.set_limit(NonZeroU8::new(3).unwrap());
/// m.set_reverse(true);
/// m.set_delimiters(Delimiters::new(".+").unwrap());
/// assert_eq!(m.to_string(), "%{D3r.+}");
/// ```
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Macro {
    kind: MacroKind,
    url_encode: bool,
    limit: Option<NonZeroU8>,
    reverse: bool,
    delimiters: Option<Delimiters>,
}

impl Macro {
    /// Creates a new macro of the given kind.
    pub fn new(kind: MacroKind) -> Macro {
        Self {
            kind,
            url_encode: Default::default(),
            limit: Default::default(),
            reverse: Default::default(),
            delimiters: Default::default(),
        }
    }

    /// Returns the macro kind.
    pub fn kind(&self) -> MacroKind {
        self.kind
    }

    /// Configures the macro’s kind.
    pub fn set_kind(&mut self, value: MacroKind) {
        self.kind = value;
    }

    /// Returns whether the macro should undergo URL encoding.
    pub fn url_encode(&self) -> bool {
        self.url_encode
    }

    /// Configures whether the macro should undergo URL encoding.
    pub fn set_url_encode(&mut self, value: bool) {
        self.url_encode = value;
    }

    /// Returns the macro’s size limit transformer.
    pub fn limit(&self) -> Option<NonZeroU8> {
        self.limit
    }

    /// Configures the macro’s size limit transformer.
    pub fn set_limit<L>(&mut self, value: L)
    where
        L: Into<Option<NonZeroU8>>,
    {
        self.limit = value.into();
    }

    /// Returns the macro’s reverse transformer.
    pub fn reverse(&self) -> bool {
        self.reverse
    }

    /// Configures the macro’s reverse transformer.
    pub fn set_reverse(&mut self, value: bool) {
        self.reverse = value;
    }

    /// Returns the macro’s delimiters.
    pub fn delimiters(&self) -> Option<&Delimiters> {
        self.delimiters.as_ref()
    }

    /// Configures the macro’s delimiters.
    pub fn set_delimiters<D>(&mut self, value: D)
    where
        D: Into<Option<Delimiters>>,
    {
        self.delimiters = value.into();
    }
}

impl Display for Macro {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "%{{")?;
        fmt_macro_letter(f, self.kind(), self.url_encode())?;
        if let Some(limit) = self.limit {
            limit.fmt(f)?;
        }
        if self.reverse {
            write!(f, "r")?;
        }
        if let Some(delimiters) = &self.delimiters {
            delimiters.0.fmt(f)?;
        }
        write!(f, "}}")
    }
}

fn fmt_macro_letter(f: &mut Formatter<'_>, kind: MacroKind, url_encode: bool) -> fmt::Result {
    let c = match kind {
        MacroKind::Sender          => if url_encode { 'S' } else { 's' },
        MacroKind::LocalPart       => if url_encode { 'L' } else { 'l' },
        MacroKind::DomainPart      => if url_encode { 'O' } else { 'o' },
        MacroKind::Domain          => if url_encode { 'D' } else { 'd' },
        MacroKind::Ip              => if url_encode { 'I' } else { 'i' },
        MacroKind::ValidatedDomain => if url_encode { 'P' } else { 'p' },
        MacroKind::HeloDomain      => if url_encode { 'H' } else { 'h' },
        MacroKind::PrettyIp        => if url_encode { 'C' } else { 'c' },
        MacroKind::Receiver        => if url_encode { 'R' } else { 'r' },
        MacroKind::Timestamp       => if url_encode { 'T' } else { 't' },
        MacroKind::VersionLabel    => if url_encode { 'V' } else { 'v' },
    };

    c.fmt(f)
}

impl From<MacroKind> for Macro {
    fn from(kind: MacroKind) -> Self {
        Macro::new(kind)
    }
}

/// The kind of a macro (*macro letter*).
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum MacroKind {
    /// Macro letter *s* (the *&lt;sender&gt;* argument).
    Sender,
    /// Macro letter *l* (the local-part of *&lt;sender&gt;*).
    LocalPart,
    /// Macro letter *o* (the domain-part of *&lt;sender&gt;*).
    DomainPart,
    /// Macro letter *d* (the *&lt;domain&gt;* argument).
    Domain,
    /// Macro letter *i* (the *&lt;ip&gt;* argument in dot format).
    Ip,
    /// Macro letter *p* (the validated domain name of *&lt;ip&gt;*).
    ValidatedDomain,
    /// Macro letter *h* (the HELO/EHLO domain).
    HeloDomain,
    /// Macro letter *c* (the presentation form of *&lt;ip&gt;*).
    PrettyIp,
    /// Macro letter *r* (the receiving host’s domain name).
    Receiver,
    /// Macro letter *t* (the number of seconds since the epoch).
    Timestamp,
    /// Macro letter *v* (the label `in-addr` or `ip6`, depending on the IP
    /// version of *&lt;ip&gt;*).
    VersionLabel,
}

/// Macro delimiter characters.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Delimiters(String);

impl Delimiters {
    /// Creates delimiters with the given value.
    ///
    /// # Errors
    ///
    /// If the given argument is invalid, it is returned as the `Err` variant.
    pub fn new<S>(s: S) -> Result<Self, String>
    where
        S: Into<String>,
    {
        let s = s.into();
        if !s.is_empty() && s.chars().all(is_delimiter_char) {
            Ok(Self(s))
        } else {
            Err(s)
        }
    }
}

fn is_delimiter_char(c: char) -> bool {
    matches!(c, '.' | '-' | '+' | ',' | '/' | '_' | '=')
}

impl Default for Delimiters {
    fn default() -> Self {
        Self(String::from("."))
    }
}

impl AsRef<str> for Delimiters {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<Delimiters> for String {
    fn from(delimiters: Delimiters) -> Self {
        delimiters.0
    }
}

/// An escape sequence in a macro string.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Escape {
    /// The percent escape sequence.
    Percent,
    /// The space escape sequence.
    Space,
    /// The URL-encoded space escape sequence.
    UrlEncodedSpace,
}

impl Display for Escape {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Percent => write!(f, "%%"),
            Self::Space => write!(f, "%_"),
            Self::UrlEncodedSpace => write!(f, "%-"),
        }
    }
}

/// A literal part of a macro string.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct MacroLiteral(String);

impl MacroLiteral {
    /// Creates a macro literal with the given value.
    ///
    /// # Errors
    ///
    /// If the given argument is invalid, it is returned as the `Err` variant.
    pub fn new<S>(s: S) -> Result<Self, String>
    where
        S: Into<String>,
    {
        let s = s.into();
        if !s.is_empty() && s.chars().all(is_macro_literal_char) {
            Ok(Self(s))
        } else {
            Err(s)
        }
    }
}

fn is_macro_literal_char(c: char) -> bool {
    matches!(c, '!'..='$' | '&'..='~')
}

impl Display for MacroLiteral {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl AsRef<str> for MacroLiteral {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<MacroLiteral> for String {
    fn from(macro_literal: MacroLiteral) -> Self {
        macro_literal.0
    }
}

fn is_alpha(c: char) -> bool {
    c.is_ascii_alphabetic()
}

fn is_digit(c: char) -> bool {
    c.is_ascii_digit()
}

fn is_alphanum(c: char) -> bool {
    c.is_ascii_alphanumeric()
}

fn is_hexdigit(c: char) -> bool {
    c.is_ascii_hexdigit()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn record_roundtrip() {
        let record = Record {
            terms: vec![
                Term::Directive(Directive {
                    qualifier: Some(Qualifier::Pass),
                    mechanism: Mechanism::A(A {
                        domain_spec: Some(
                            DomainSpec::new(MacroString {
                                segments: vec![MacroStringSegment::MacroLiteral(
                                    MacroLiteral::new("_spf.gluet.ch").unwrap(),
                                )],
                            })
                            .unwrap(),
                        ),
                        prefix_len: Some(DualCidrLength::Both(
                            Ip4CidrLength::new(24).unwrap(),
                            Ip6CidrLength::new(64).unwrap(),
                        )),
                    }),
                }),
                Term::Modifier(Modifier::Redirect(Redirect {
                    domain_spec: DomainSpec::new(MacroString {
                        segments: vec![MacroStringSegment::MacroLiteral(
                            MacroLiteral::new("test.gluet.ch").unwrap(),
                        )],
                    })
                    .unwrap(),
                })),
            ],
        };

        let s = record.to_string();

        assert_eq!(s, "v=spf1 +a:_spf.gluet.ch/24//64 redirect=test.gluet.ch");

        assert_eq!(record, s.parse().unwrap());
    }

    #[test]
    fn record_match_destructuring() {
        let record = "v=spf1 +a:myhost.ch/24".parse::<Record>().unwrap();

        let ch_tld = match record.into_iter().next() {
            Some(Term::Directive(directive)) => match directive.mechanism {
                Mechanism::A(a) => match a.domain_spec {
                    Some(domain_spec) => {
                        let macro_string = MacroString::from(domain_spec);
                        match macro_string.segments.into_iter().next() {
                            Some(MacroStringSegment::MacroLiteral(literal)) => {
                                literal.as_ref().ends_with(".ch")
                            }
                            _ => false,
                        }
                    }
                    None => false,
                },
                _ => false,
            },
            _ => false,
        };

        assert!(ch_tld);
    }

    #[test]
    fn record_display_ok() {
        let s = "v=spf1 a:wh%_is%{Sr}.com";
        assert_eq!(s.parse::<Record>().unwrap().to_string(), s);
    }

    #[test]
    fn ip6_addr_display_ok() {
        let ip6 = Ip6 {
            addr: Ipv6Addr::new(0xff00, 0, 0, 0, 0, 0, 0, 0),
            prefix_len: None,
        };
        assert_eq!(ip6.to_string(), "ip6:ff00::");

        let ip6 = Ip6 {
            addr: Ipv6Addr::new(0, 0, 0, 0, 0, 0xffff, 0xc00a, 0x2ff),
            prefix_len: None,
        };
        assert_eq!(ip6.to_string(), "ip6:::ffff:192.10.2.255");
    }

    #[test]
    fn ip4_cidr_length_ok() {
        assert_eq!(Ip4CidrLength::new(0).map(From::from), Some(0));
        assert_eq!(Ip4CidrLength::new(1).map(From::from), Some(1));
        assert_eq!(Ip4CidrLength::new(28).map(From::from), Some(28));
        assert_eq!(Ip4CidrLength::new(32).map(From::from), Some(32));
        assert_eq!(Ip4CidrLength::new(33), None);
    }

    #[test]
    fn domain_spec_multiple_final_literals() {
        let ms = vec![
            MacroStringSegment::MacroLiteral(MacroLiteral::new("example.org.").unwrap()),
        ];
        assert!(DomainSpec::new(ms).is_ok());

        let ms = vec![
            MacroStringSegment::MacroLiteral(MacroLiteral::new("example.").unwrap()),
            MacroStringSegment::MacroLiteral(MacroLiteral::new("or").unwrap()),
            MacroStringSegment::MacroLiteral(MacroLiteral::new("g.").unwrap()),
        ];
        assert!(DomainSpec::new(ms).is_ok());
    }

    #[test]
    fn explain_string_display_ok() {
        // Example explanation from §6.2.
        let s = "See http://%{d}/why.html?s=%{S}&i=%{I}";
        assert_eq!(s.parse::<ExplainString>().unwrap().to_string(), s);
    }

    #[test]
    fn macro_expand_display_ok() {
        assert_eq!(MacroExpand::Escape(Escape::Percent).to_string(), "%%");
    }
}
