// viaspf – implementation of the SPF specification
// Copyright © 2020–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

mod util;

use self::util::*;
use super::*;
use std::{
    net::{Ipv4Addr, Ipv6Addr},
    num::NonZeroU8,
    str::FromStr,
};

impl FromStr for Record {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match record(s) {
            Some((i, result)) if i.is_empty() => Ok(result),
            _ => Err(ParseError),
        }
    }
}

fn record(input: &str) -> Option<(&str, Record)> {
    let i = input.strip_prefix_ignore_case("v=spf1")?;
    let (i, terms) = i.collect_many(term).unwrap_or_default_result(i);
    let i = i.scan_many(|i| i.strip_prefix(is_space)).unwrap_or(i);
    Some((i, Record { terms }))
}

fn term(input: &str) -> Option<(&str, Term)> {
    let i = input.scan_many(|i| i.strip_prefix(is_space))?;
    directive(i)
        .map_result(Term::Directive)
        .or_else(|| modifier(i).map_result(Term::Modifier))
}

fn directive(input: &str) -> Option<(&str, Directive)> {
    let (i, qualifier) = qualifier(input).unwrap_or_none_result(input);
    let (i, mechanism) = mechanism(i)?;
    Some((i, Directive { qualifier, mechanism }))
}

fn qualifier(input: &str) -> Option<(&str, Qualifier)> {
    input.strip_char(&['+', '-', '?', '~']).map_result(|c| match c {
        '+' => Qualifier::Pass,
        '-' => Qualifier::Fail,
        '?' => Qualifier::Neutral,
        '~' => Qualifier::Softfail,
        _ => unreachable!(),
    })
}

fn mechanism(input: &str) -> Option<(&str, Mechanism)> {
    mechanism_all(input)
        .or_else(|| mechanism_include(input))
        .or_else(|| mechanism_a(input))
        .or_else(|| mechanism_mx(input))
        .or_else(|| mechanism_ptr(input))
        .or_else(|| mechanism_ip4(input))
        .or_else(|| mechanism_ip6(input))
        .or_else(|| mechanism_exists(input))
}

fn mechanism_all(input: &str) -> Option<(&str, Mechanism)> {
    input.strip_prefix_ignore_case("all").map(|i| (i, Mechanism::All))
}

fn mechanism_include(input: &str) -> Option<(&str, Mechanism)> {
    input
        .strip_prefix_ignore_case("include:")
        .and_then(domain_spec)
        .map_result(|domain_spec| Mechanism::Include(Include { domain_spec }))
}

fn mechanism_a(input: &str) -> Option<(&str, Mechanism)> {
    let i = input.strip_prefix_ignore_case("a")?;
    let (i, domain_spec) = i.strip_prefix(':').and_then(domain_spec).unwrap_or_none_result(i);
    let (i, prefix_len) = dual_cidr_length(i).unwrap_or_none_result(i);
    Some((i, Mechanism::A(A { domain_spec, prefix_len })))
}

fn mechanism_mx(input: &str) -> Option<(&str, Mechanism)> {
    let i = input.strip_prefix_ignore_case("mx")?;
    let (i, domain_spec) = i.strip_prefix(':').and_then(domain_spec).unwrap_or_none_result(i);
    let (i, prefix_len) = dual_cidr_length(i).unwrap_or_none_result(i);
    Some((i, Mechanism::Mx(Mx { domain_spec, prefix_len })))
}

fn mechanism_ptr(input: &str) -> Option<(&str, Mechanism)> {
    let i = input.strip_prefix_ignore_case("ptr")?;
    let (i, domain_spec) = i.strip_prefix(':').and_then(domain_spec).unwrap_or_none_result(i);
    Some((i, Mechanism::Ptr(Ptr { domain_spec })))
}

fn mechanism_ip4(input: &str) -> Option<(&str, Mechanism)> {
    let i = input.strip_prefix_ignore_case("ip4:")?;
    let (i, addr) = ip4_network(i)?;
    let (i, prefix_len) = ip4_cidr_length(i).unwrap_or_none_result(i);
    Some((i, Mechanism::Ip4(Ip4 { addr, prefix_len })))
}

fn mechanism_ip6(input: &str) -> Option<(&str, Mechanism)> {
    let i = input.strip_prefix_ignore_case("ip6:")?;
    let (i, addr) = ip6_network(i)?;
    let (i, prefix_len) = ip6_cidr_length(i).unwrap_or_none_result(i);
    Some((i, Mechanism::Ip6(Ip6 { addr, prefix_len })))
}

fn mechanism_exists(input: &str) -> Option<(&str, Mechanism)> {
    input
        .strip_prefix_ignore_case("exists:")
        .and_then(domain_spec)
        .map_result(|domain_spec| Mechanism::Exists(Exists { domain_spec }))
}

fn modifier(input: &str) -> Option<(&str, Modifier)> {
    modifier_redirect(input)
        .or_else(|| modifier_explanation(input))
        .or_else(|| modifier_unknown(input))
}

fn modifier_redirect(input: &str) -> Option<(&str, Modifier)> {
    input
        .strip_prefix_ignore_case("redirect=")
        .and_then(domain_spec)
        .map_result(|domain_spec| Modifier::Redirect(Redirect { domain_spec }))
}

fn modifier_explanation(input: &str) -> Option<(&str, Modifier)> {
    input
        .strip_prefix_ignore_case("exp=")
        .and_then(domain_spec)
        .map_result(|domain_spec| Modifier::Explanation(Explanation { domain_spec }))
}

fn modifier_unknown(input: &str) -> Option<(&str, Modifier)> {
    let (i, name) = name(input).filter_result(is_unknown_modifier_name)?;
    let i = i.strip_prefix('=')?;
    let (i, value) = macro_string(i)?;
    Some((i, Modifier::Unknown(Unknown { name, value })))
}

fn name(input: &str) -> Option<(&str, Name)> {
    let i = input
        .scan_many(|i| i.strip_prefix(is_alpha))
        .map(|i| i.scan_many(|i| i.strip_prefix(is_name_char)).unwrap_or(i))?;
    Some((i, Name(input.without_suffix(i).to_owned())))
}

fn ip4_network(input: &str) -> Option<(&str, Ipv4Addr)> {
    let (i, a) = qnum(input)?;
    let i = i.strip_prefix('.')?;
    let (i, b) = qnum(i)?;
    let i = i.strip_prefix('.')?;
    let (i, c) = qnum(i)?;
    let i = i.strip_prefix('.')?;
    let (i, d) = qnum(i)?;
    Some((i, Ipv4Addr::new(a, b, c, d)))
}

fn qnum(input: &str) -> Option<(&str, u8)> {
    let i = input
        .strip_prefix("25")
        .and_then(|i| i.strip_prefix(is_digit_up_to_5))
        .or_else(|| {
            input
                .strip_prefix('2')
                .and_then(|i| i.strip_prefix(is_digit_up_to_4))
                .and_then(|i| i.strip_prefix(is_digit))
        })
        .or_else(|| {
            input
                .strip_prefix('1')
                .and_then(|i| i.strip_prefix(is_digit))
                .and_then(|i| i.strip_prefix(is_digit))
        })
        .or_else(|| {
            input
                .strip_prefix(is_positive_digit)
                .and_then(|i| i.strip_prefix(is_digit))
        })
        .or_else(|| input.strip_prefix(is_digit))?;
    input.without_suffix(i).parse().ok().map(|n| (i, n))
}

fn is_digit_up_to_5(c: char) -> bool {
    matches!(c, '0'..='5')
}

fn is_digit_up_to_4(c: char) -> bool {
    matches!(c, '0'..='4')
}

fn ip6_network(mut input: &str) -> Option<(&str, Ipv6Addr)> {
    let (mut hi, mut lo) = (Vec::with_capacity(8), Vec::with_capacity(8));

    if let Some((i, piece)) = ip6_piece(input) {
        input = i;
        hi.push(piece);

        if let Some((i, pieces)) =
            input.collect_many_n(5, |i| i.strip_prefix(':').and_then(ip6_piece))
        {
            input = i;
            hi.extend(pieces);

            if hi.len() == 6 {
                if let Some((i, addr)) = input.strip_prefix(':').and_then(ip4_network) {
                    return Some((i, ipv6_addr_from_pieces_ipv4(hi, lo, addr)));
                }

                if let Some((i, pieces)) =
                    input.collect_many_n(2, |i| i.strip_prefix(':').and_then(ip6_piece))
                {
                    input = i;
                    hi.extend(pieces);

                    if hi.len() == 8 {
                        return Some((input, ipv6_addr_from_pieces(hi, lo)));
                    }
                }
            }
        }
    }

    let mut input = input.strip_prefix("::")?;

    if hi.len() <= 5 {
        if let Some((i, addr)) = ip4_network(input) {
            return Some((i, ipv6_addr_from_pieces_ipv4(hi, lo, addr)));
        }
    }

    if hi.len() <= 6 {
        if let Some((i, piece)) = ip6_piece(input) {
            input = i;
            lo.push(piece);

            for n in (hi.len() + 2)..8 {
                if n <= 6 {
                    if let Some((i, addr)) = input.strip_prefix(':').and_then(ip4_network) {
                        return Some((i, ipv6_addr_from_pieces_ipv4(hi, lo, addr)));
                    }
                }

                if let Some((i, piece)) = input.strip_prefix(':').and_then(ip6_piece) {
                    input = i;
                    lo.push(piece);
                } else {
                    break;
                }
            }
        }
    }

    Some((input, ipv6_addr_from_pieces(hi, lo)))
}

fn ip6_piece(input: &str) -> Option<(&str, u16)> {
    let i = input.scan_many_n(4, |i| i.strip_prefix(is_hexdigit))?;
    u16::from_str_radix(input.without_suffix(i), 16).ok().map(|n| (i, n))
}

fn ipv6_addr_from_pieces_ipv4(hi: Vec<u16>, mut lo: Vec<u16>, addr: Ipv4Addr) -> Ipv6Addr {
    assert!(hi.len() <= 6 && lo.len() <= 5);
    assert!(hi.len() + lo.len() <= 6);

    let [a, b, c, d] = u32::from(addr).to_be_bytes();
    lo.push(u16::from_be_bytes([a, b]));
    lo.push(u16::from_be_bytes([c, d]));

    ipv6_addr_from_pieces(hi, lo)
}

fn ipv6_addr_from_pieces(mut hi: Vec<u16>, lo: Vec<u16>) -> Ipv6Addr {
    assert!(hi.len() <= 8 && lo.len() <= 7);
    assert!(hi.len() + lo.len() <= 8);

    hi.resize_with(8 - lo.len(), Default::default);
    hi.extend(lo);

    assert_eq!(hi.len(), 8);

    match *hi {
        [a, b, c, d, e, f, g, h] => Ipv6Addr::new(a, b, c, d, e, f, g, h),
        _ => unreachable!(),
    }
}

fn dual_cidr_length(input: &str) -> Option<(&str, DualCidrLength)> {
    fn dual_cidr_length_both(input: &str) -> Option<(&str, DualCidrLength)> {
        let (i, len4) = ip4_cidr_length(input)?;
        let i = i.strip_prefix('/')?;
        let (i, len6) = ip6_cidr_length(i)?;
        Some((i, DualCidrLength::Both(len4, len6)))
    }

    dual_cidr_length_both(input)
        .or_else(|| ip4_cidr_length(input).map_result(DualCidrLength::Ip4))
        .or_else(|| {
            input
                .strip_prefix('/')
                .and_then(ip6_cidr_length)
                .map_result(DualCidrLength::Ip6)
        })
}

fn ip4_cidr_length(input: &str) -> Option<(&str, Ip4CidrLength)> {
    input.strip_prefix('/').and_then(length32).map_result(Ip4CidrLength)
}

fn length32(input: &str) -> Option<(&str, u8)> {
    let i = input
        .strip_prefix('0')
        .or_else(|| {
            input
                .strip_prefix(is_positive_digit)
                .map(|i| i.strip_prefix(is_digit).unwrap_or(i))
        })?;
    input.without_suffix(i).parse().ok().filter(is_ip4_cidr_length).map(|n| (i, n))
}

fn ip6_cidr_length(input: &str) -> Option<(&str, Ip6CidrLength)> {
    input.strip_prefix('/').and_then(length128).map_result(Ip6CidrLength)
}

fn length128(input: &str) -> Option<(&str, u8)> {
    let i = input
        .strip_prefix('0')
        .or_else(|| {
            input
                .strip_prefix(is_positive_digit)
                .map(|i| i.scan_many_n(2, |i| i.strip_prefix(is_digit)).unwrap_or(i))
        })?;
    input.without_suffix(i).parse().ok().filter(is_ip6_cidr_length).map(|n| (i, n))
}

fn is_positive_digit(c: char) -> bool {
    matches!(c, '1'..='9')
}

fn domain_spec(input: &str) -> Option<(&str, DomainSpec)> {
    // A ‘domain-spec’ is first parsed as a macro string. However, the macro
    // string must end with ‘domain-end’ and so may have consumed too much of
    // the input. This implements backtracking through the final segments of the
    // macro string to find a ‘domain-end’.

    let (i, mut macro_string) = macro_string(input)?;
    let len = input.len() - i.len();

    match &mut macro_string.segments[..] {
        [.., MacroStringSegment::MacroExpand(_)] => Some((i, DomainSpec(macro_string))),
        [segments @ .., MacroStringSegment::MacroLiteral(literal)] => {
            // Backtracking takes care of inputs such as `"example.org/30"`,
            // where `macro_string` consumes everything, including the CIDR
            // prefix length.

            let s = literal.as_ref();

            // Look backwards through the literal for a `.` which begins a
            // ‘domain-end’ label.
            let mut snext = s;
            while let Some(index) = snext.rfind('.') {
                if let Some((ix, _)) = domain_end(&s[index..]) {
                    let i = &input[(len - ix.len())..];

                    let slen = s.len() - ix.len();
                    literal.0.truncate(slen);

                    return Some((i, DomainSpec(macro_string)));
                }
                snext = &snext[..index];
            }

            // The final literal contains no acceptable ‘domain-end’ label. Fall
            // back to a preceding ‘macro-expand’, if one is present.
            if let Some(MacroStringSegment::MacroExpand(_)) = segments.last() {
                let i = &input[(len - s.len())..];

                let slen = segments.len();
                macro_string.segments.truncate(slen);

                return Some((i, DomainSpec(macro_string)));
            }

            None
        }
        _ => None,
    }
}

fn domain_end(input: &str) -> Option<(&str, &str)> {
    let i = input.strip_prefix('.')?;
    let (i, toplabel) = toplabel(i).filter_result(|t| is_toplabel(t))?;
    let i = i.strip_prefix('.').unwrap_or(i);
    Some((i, toplabel))
}

fn toplabel(input: &str) -> Option<(&str, &str)> {
    fn strip_alphanumeric(input: &str) -> Option<&str> {
        input.scan_many(|i| i.strip_prefix(is_alphanum))
    }

    let i = strip_alphanumeric(input)
        .and_then(|i| {
            i.scan_many(|i| {
                i.scan_many(|i| i.strip_prefix('-'))
                    .and_then(strip_alphanumeric)
            })
        })
        .or_else(|| {
            let i = input.scan_many(|i| i.strip_prefix(is_digit)).unwrap_or(input);
            i.scan_many(|i| i.strip_prefix(is_alpha))
                .map(|i| strip_alphanumeric(i).unwrap_or(i))
        })?;
    Some((i, input.without_suffix(i)))
}

impl FromStr for ExplainString {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match explain_string(s) {
            Some((i, result)) if i.is_empty() => Ok(result),
            _ => Err(ParseError),
        }
    }
}

fn explain_string(input: &str) -> Option<(&str, ExplainString)> {
    let (i, segments) = input.collect_many(explain_string_segment).unwrap_or_default_result(input);
    Some((i, ExplainString { segments }))
}

fn explain_string_segment(input: &str) -> Option<(&str, ExplainStringSegment)> {
    input
        .strip_prefix(is_space)
        .map(|i| (i, ExplainStringSegment::Space))
        .or_else(|| {
            input
                .collect_many(macro_string_segment)
                .map_result(|segments| ExplainStringSegment::MacroString(MacroString { segments }))
        })
}

impl FromStr for MacroString {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match macro_string(s) {
            Some((i, result)) if i.is_empty() => Ok(result),
            _ => Err(ParseError),
        }
    }
}

fn macro_string(input: &str) -> Option<(&str, MacroString)> {
    let (i, segments) = input.collect_many(macro_string_segment).unwrap_or_default_result(input);
    Some((i, MacroString { segments }))
}

fn macro_string_segment(input: &str) -> Option<(&str, MacroStringSegment)> {
    macro_expand(input)
        .map_result(MacroStringSegment::MacroExpand)
        .or_else(|| macro_literal(input).map_result(MacroStringSegment::MacroLiteral))
}

fn macro_expand(input: &str) -> Option<(&str, MacroExpand)> {
    let i = input.strip_prefix('%')?;
    macro_(i)
        .map_result(MacroExpand::Macro)
        .or_else(|| macro_escape(i).map_result(MacroExpand::Escape))
}

fn macro_(input: &str) -> Option<(&str, Macro)> {
    let i = input.strip_prefix('{')?;
    let (i, (kind, url_encode)) = macro_letter(i)?;
    let (i, limit) = macro_transformer_limit(i).unwrap_or_none_result(i);
    let (i, reverse) = macro_transformer_reverse(i)?;
    let (i, delimiters) = macro_delimiters(i).unwrap_or_none_result(i);
    let i = i.strip_prefix('}')?;
    Some((
        i,
        Macro {
            kind,
            url_encode,
            limit,
            reverse,
            delimiters,
        },
    ))
}

fn macro_letter(input: &str) -> Option<(&str, (MacroKind, bool))> {
    fn to_macro_kind(c: char) -> MacroKind {
        match c {
            's' | 'S' => MacroKind::Sender,
            'l' | 'L' => MacroKind::LocalPart,
            'o' | 'O' => MacroKind::DomainPart,
            'd' | 'D' => MacroKind::Domain,
            'i' | 'I' => MacroKind::Ip,
            'p' | 'P' => MacroKind::ValidatedDomain,
            'h' | 'H' => MacroKind::HeloDomain,
            'c' | 'C' => MacroKind::PrettyIp,
            'r' | 'R' => MacroKind::Receiver,
            't' | 'T' => MacroKind::Timestamp,
            'v' | 'V' => MacroKind::VersionLabel,
            _ => unreachable!(),
        }
    }

    input
        .strip_char(&['s', 'l', 'o', 'd', 'i', 'p', 'h', 'c', 'r', 't', 'v'])
        .map_result(|c| (to_macro_kind(c), false))
        .or_else(|| {
            input
                .strip_char(&['S', 'L', 'O', 'D', 'I', 'P', 'H', 'C', 'R', 'T', 'V'])
                .map_result(|c| (to_macro_kind(c), true))
        })
}

fn macro_transformer_limit(input: &str) -> Option<(&str, NonZeroU8)> {
    let i = input.scan_many(|i| i.strip_prefix(is_digit))?;
    input.without_suffix(i).parse().ok().map(|n| (i, n))
}

fn macro_transformer_reverse(input: &str) -> Option<(&str, bool)> {
    match input.strip_prefix_ignore_case("r") {
        None => Some((input, false)),
        Some(i) => Some((i, true)),
    }
}

fn macro_delimiters(input: &str) -> Option<(&str, Delimiters)> {
    let i = input.scan_many(|i| i.strip_prefix(is_delimiter_char))?;
    Some((i, Delimiters(input.without_suffix(i).to_owned())))
}

fn macro_escape(input: &str) -> Option<(&str, Escape)> {
    input.strip_char(&['%', '_', '-']).map_result(|c| match c {
        '%' => Escape::Percent,
        '_' => Escape::Space,
        '-' => Escape::UrlEncodedSpace,
        _ => unreachable!(),
    })
}

fn macro_literal(input: &str) -> Option<(&str, MacroLiteral)> {
    let i = input.scan_many(|i| i.strip_prefix(is_macro_literal_char))?;
    Some((i, MacroLiteral(input.without_suffix(i).to_owned())))
}

fn is_space(c: char) -> bool {
    c == ' '
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn record_ok() {
        assert_eq!(record("v=Spf1 "), Some(("", Record { terms: vec![] })));
        assert_eq!(
            record("v=Spf1  +A  -All  "),
            Some((
                "",
                Record {
                    terms: vec![
                        Term::Directive(Directive {
                            qualifier: Some(Qualifier::Pass),
                            mechanism: Mechanism::A(A {
                                domain_spec: None,
                                prefix_len: None,
                            }),
                        }),
                        Term::Directive(Directive {
                            qualifier: Some(Qualifier::Fail),
                            mechanism: Mechanism::All,
                        }),
                    ]
                }
            ))
        );
        assert_eq!(record("v=Spf1 trailer"), Some(("trailer", Record { terms: vec![] })));
        assert_eq!(record("v=Spf10 "), Some(("0 ", Record { terms: vec![] })));
        assert_eq!(record("v=Spf2 "), None);
    }

    #[test]
    fn term_ok() {
        assert_eq!(
            term("  -All "),
            Some((
                " ",
                Term::Directive(Directive {
                    qualifier: Some(Qualifier::Fail),
                    mechanism: Mechanism::All,
                })
            ))
        );
        assert_eq!(term(" "), None);
    }

    #[test]
    fn directive_ok() {
        assert_eq!(
            directive("+All "),
            Some((
                " ",
                Directive {
                    qualifier: Some(Qualifier::Pass),
                    mechanism: Mechanism::All,
                }
            ))
        );
        assert_eq!(
            directive("All "),
            Some((
                " ",
                Directive {
                    qualifier: None,
                    mechanism: Mechanism::All,
                }
            ))
        );
        assert_eq!(directive("–All "), None);
        assert_eq!(directive(" "), None);
    }

    #[test]
    fn qualifier_ok() {
        assert_eq!(qualifier("? "), Some((" ", Qualifier::Neutral)));
        assert_eq!(qualifier(" "), None);
    }

    #[test]
    fn mechanism_ok() {
        assert_eq!(
            mechanism("Ptr:example.org "),
            Some((
                " ",
                Mechanism::Ptr(Ptr {
                    domain_spec: Some(DomainSpec(MacroString {
                        segments: vec![MacroStringSegment::MacroLiteral(
                            MacroLiteral::new("example.org").unwrap()
                        )]
                    })),
                })
            ))
        );
        assert_eq!(mechanism(" "), None);
    }

    #[test]
    fn mechanism_all_ok() {
        assert_eq!(mechanism_all("ALL "), Some((" ", Mechanism::All)));
        assert_eq!(mechanism_all("Alle "), Some(("e ", Mechanism::All)));
        assert_eq!(mechanism_all("Al "), None);
    }

    #[test]
    fn mechanism_mx_ok() {
        assert_eq!(
            mechanism_mx("Mx "),
            Some((
                " ",
                Mechanism::Mx(Mx {
                    domain_spec: None,
                    prefix_len: None,
                })
            ))
        );
        assert_eq!(
            mechanism_mx("Mx:example.org/22 "),
            Some((
                " ",
                Mechanism::Mx(Mx {
                    domain_spec: Some(DomainSpec(MacroString {
                        segments: vec![MacroStringSegment::MacroLiteral(
                            MacroLiteral::new("example.org").unwrap()
                        )]
                    })),
                    prefix_len: Some(DualCidrLength::Ip4(Ip4CidrLength::new(22).unwrap())),
                })
            ))
        );
        assert_eq!(
            mechanism_mx("Mx:%{d}//122 "),
            Some((
                " ",
                Mechanism::Mx(Mx {
                    domain_spec: Some(DomainSpec(MacroString {
                        segments: vec![MacroStringSegment::MacroExpand(MacroExpand::Macro(
                            Macro::new(MacroKind::Domain)
                        ))]
                    })),
                    prefix_len: Some(DualCidrLength::Ip6(Ip6CidrLength::new(122).unwrap())),
                })
            ))
        );
        assert_eq!(
            mechanism_mx("Mx/28 "),
            Some((
                " ",
                Mechanism::Mx(Mx {
                    domain_spec: None,
                    prefix_len: Some(DualCidrLength::Ip4(Ip4CidrLength::new(28).unwrap())),
                })
            ))
        );
        assert_eq!(mechanism_mx(" "), None);
    }

    #[test]
    fn mechanism_ip4_ok() {
        assert_eq!(
            mechanism_ip4("Ip4:1.0.0.2 "),
            Some((
                " ",
                Mechanism::Ip4(Ip4 {
                    addr: Ipv4Addr::new(1, 0, 0, 2),
                    prefix_len: None,
                })
            ))
        );
        assert_eq!(
            mechanism_ip4("Ip4:1.0.0.2/28 "),
            Some((
                " ",
                Mechanism::Ip4(Ip4 {
                    addr: Ipv4Addr::new(1, 0, 0, 2),
                    prefix_len: Some(Ip4CidrLength::new(28).unwrap()),
                })
            ))
        );
        assert_eq!(mechanism_ip4(" "), None);
    }

    #[test]
    fn mechanism_ip6_ok() {
        assert_eq!(
            mechanism_ip6("Ip6:1::3 "),
            Some((
                " ",
                Mechanism::Ip6(Ip6 {
                    addr: Ipv6Addr::new(0x1, 0, 0, 0, 0, 0, 0, 0x3),
                    prefix_len: None,
                })
            ))
        );
        assert_eq!(
            mechanism_ip6("Ip6:1::3/116 "),
            Some((
                " ",
                Mechanism::Ip6(Ip6 {
                    addr: Ipv6Addr::new(0x1, 0, 0, 0, 0, 0, 0, 0x3),
                    prefix_len: Some(Ip6CidrLength::new(116).unwrap()),
                })
            ))
        );
        assert_eq!(mechanism_ip6(" "), None);
    }

    #[test]
    fn modifier_ok() {
        assert_eq!(
            modifier("Redirect=a.org "),
            Some((
                " ",
                Modifier::Redirect(Redirect {
                    domain_spec: DomainSpec(MacroString {
                        segments: vec![MacroStringSegment::MacroLiteral(
                            MacroLiteral::new("a.org").unwrap()
                        )]
                    })
                })
            ))
        );
        assert_eq!(modifier("Exp= "), None);
        assert_eq!(modifier(" "), None);
    }

    #[test]
    fn modifier_unknown_ok() {
        assert_eq!(
            modifier_unknown("Ab.c34=%{d} "),
            Some((
                " ",
                Modifier::Unknown(
                    Unknown::new(
                        Name::new("Ab.c34").unwrap(),
                        MacroString {
                            segments: vec![MacroStringSegment::MacroExpand(MacroExpand::Macro(
                                Macro::new(MacroKind::Domain)
                            ))]
                        }
                    )
                    .unwrap()
                )
            ))
        );
        assert_eq!(
            modifier_unknown("Ab= "),
            Some((
                " ",
                Modifier::Unknown(
                    Unknown::new(
                        Name::new("Ab").unwrap(),
                        MacroString { segments: vec![] }
                    )
                    .unwrap()
                )
            ))
        );
        assert_eq!(modifier_unknown("Exp= "), None);
        assert_eq!(modifier_unknown("1= "), None);
    }

    #[test]
    fn ip4_network_ok() {
        assert_eq!(ip4_network("255.244.133.0 "), Some((" ", Ipv4Addr::new(255, 244, 133, 0))));
        assert_eq!(ip4_network("9.87.6.5 "), Some((" ", Ipv4Addr::new(9, 87, 6, 5))));
        assert_eq!(ip4_network("256.7.8.9 "), None);
        assert_eq!(ip4_network("1.02.3.4 "), None);
        assert_eq!(ip4_network("1.2.3. "), None);
        assert_eq!(ip4_network(" "), None);
    }

    #[test]
    fn ip6_network_ok() {
        assert_eq!(
            ip6_network("1:1:1:1:1:1:127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 1, 1, 1, 1, 1, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1:1:1:1:1:1:1:1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 1, 1, 1, 1, 1, 1, 1)))
        );
        assert_eq!(
            ip6_network("1:1:1:1:1:1:1::9 "),
            Some(("9 ", Ipv6Addr::new(1, 1, 1, 1, 1, 1, 1, 0)))
        );
        assert_eq!(
            ip6_network("1:1:1:1:1:1:: "),
            Some((" ", Ipv6Addr::new(1, 1, 1, 1, 1, 1, 0, 0)))
        );
        assert_eq!(
            ip6_network("1:: "),
            Some((" ", Ipv6Addr::new(1, 0, 0, 0, 0, 0, 0, 0)))
        );
        assert_eq!(
            ip6_network(":: "),
            Some((" ", Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0)))
        );
        assert_eq!(
            ip6_network("::127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1::127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 0, 0, 0, 0, 0, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1:1:1:1:1::127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 1, 1, 1, 1, 0, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1::1 "),
            Some((" ", Ipv6Addr::new(1, 0, 0, 0, 0, 0, 0, 1)))
        );
        assert_eq!(
            ip6_network("1:1::1:1:1 "),
            Some((" ", Ipv6Addr::new(1, 1, 0, 0, 0, 1, 1, 1)))
        );
        assert_eq!(
            ip6_network("::1:127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(0, 0, 0, 0, 0, 1, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1:1::1:1:1:127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 1, 0, 1, 1, 1, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("::1:1:1:1:1:127.0.0.1:9 "),
            Some((":9 ", Ipv6Addr::new(0, 1, 1, 1, 1, 1, 0x7f00, 1)))
        );
        assert_eq!(
            ip6_network("1:1::1:1:1:1:1:9 "),
            Some((":9 ", Ipv6Addr::new(1, 1, 0, 1, 1, 1, 1, 1)))
        );
        assert_eq!(
            ip6_network("2a00:1128:200:4:0:56ff:fee8:6a5a "),
            Some((" ", Ipv6Addr::new(0x2a00, 0x1128, 0x200, 0x4, 0, 0x56ff, 0xfee8, 0x6a5a)))
        );
        assert_eq!(ip6_network(" "), None);
    }

    #[test]
    fn dual_cidr_length_ok() {
        assert_eq!(
            dual_cidr_length("/0//0 "),
            Some((
                " ",
                DualCidrLength::Both(
                    Ip4CidrLength::new(0).unwrap(),
                    Ip6CidrLength::new(0).unwrap()
                )
            ))
        );
        assert_eq!(
            dual_cidr_length("/12//34 "),
            Some((
                " ",
                DualCidrLength::Both(
                    Ip4CidrLength::new(12).unwrap(),
                    Ip6CidrLength::new(34).unwrap()
                )
            ))
        );
        assert_eq!(
            dual_cidr_length("/12// "),
            Some(("// ", DualCidrLength::Ip4(Ip4CidrLength::new(12).unwrap())))
        );
        assert_eq!(
            dual_cidr_length("//78 "),
            Some((" ", DualCidrLength::Ip6(Ip6CidrLength::new(78).unwrap())))
        );
        assert_eq!(dual_cidr_length("/ "), None);
        assert_eq!(dual_cidr_length(" "), None);
    }

    #[test]
    fn ip4_cidr_length_ok() {
        assert_eq!(ip4_cidr_length("/0 "), Some((" ", Ip4CidrLength::new(0).unwrap())));
        assert_eq!(ip4_cidr_length("/1 "), Some((" ", Ip4CidrLength::new(1).unwrap())));
        assert_eq!(ip4_cidr_length("/02 "), Some(("2 ", Ip4CidrLength::new(0).unwrap())));
        assert_eq!(ip4_cidr_length("/20 "), Some((" ", Ip4CidrLength::new(20).unwrap())));
        assert_eq!(ip4_cidr_length("/45 "), None);
        assert_eq!(ip4_cidr_length(" "), None);
    }

    #[test]
    fn ip6_cidr_length_ok() {
        assert_eq!(ip6_cidr_length("/0 "), Some((" ", Ip6CidrLength::new(0).unwrap())));
        assert_eq!(ip6_cidr_length("/1 "), Some((" ", Ip6CidrLength::new(1).unwrap())));
        assert_eq!(ip6_cidr_length("/02 "), Some(("2 ", Ip6CidrLength::new(0).unwrap())));
        assert_eq!(ip6_cidr_length("/12 "), Some((" ", Ip6CidrLength::new(12).unwrap())));
        assert_eq!(ip6_cidr_length("/123 "), Some((" ", Ip6CidrLength::new(123).unwrap())));
        assert_eq!(ip6_cidr_length("/1234 "), Some(("4 ", Ip6CidrLength::new(123).unwrap())));
        assert_eq!(ip6_cidr_length("/129 "), None);
        assert_eq!(ip6_cidr_length(" "), None);
    }

    #[test]
    fn domain_spec_ok() {
        assert_eq!(
            domain_spec("mail.%{d3} "),
            Some((
                " ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroLiteral(MacroLiteral::new("mail.").unwrap()),
                        MacroStringSegment::MacroExpand(MacroExpand::Macro({
                            let mut m = Macro::new(MacroKind::Domain);
                            m.set_limit(NonZeroU8::new(3).unwrap());
                            m
                        })),
                    ]
                })
                .unwrap()
            ))
        );

        let mut ip_macro = Macro::new(MacroKind::Ip);
        ip_macro.set_reverse(true);

        assert_eq!(
            domain_spec("%{ir}.example.org "),
            Some((
                " ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(
                            MacroLiteral::new(".example.org").unwrap()
                        ),
                    ]
                })
                .unwrap()
            ))
        );
        assert_eq!(
            domain_spec("%{ir}.example.org. "),
            Some((
                " ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(
                            MacroLiteral::new(".example.org.").unwrap()
                        ),
                    ]
                })
                .unwrap()
            ))
        );
        assert_eq!(
            domain_spec("%{ir}.example.org/24 "),
            Some((
                "/24 ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(
                            MacroLiteral::new(".example.org").unwrap()
                        ),
                    ]
                })
                .unwrap()
            ))
        );
        assert_eq!(
            domain_spec("%{ir}.example.org./24 "),
            Some((
                "/24 ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(
                            MacroLiteral::new(".example.org.").unwrap()
                        ),
                    ]
                })
                .unwrap()
            ))
        );

        assert_eq!(
            domain_spec("%{ir}.example.1312 "),
            Some((
                "1312 ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(MacroLiteral::new(".example.").unwrap()),
                    ]
                })
                .unwrap()
            ))
        );
        assert_eq!(
            domain_spec("%{ir}.example.1312./24 "),
            Some((
                "1312./24 ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(ip_macro.clone())),
                        MacroStringSegment::MacroLiteral(MacroLiteral::new(".example.").unwrap()),
                    ]
                })
                .unwrap()
            ))
        );

        assert_eq!(
            domain_spec("mail.%{d}/24 "),
            Some((
                "/24 ",
                DomainSpec::new(MacroString {
                    segments: vec![
                        MacroStringSegment::MacroLiteral(MacroLiteral::new("mail.").unwrap()),
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(Macro::new(
                            MacroKind::Domain
                        ))),
                    ]
                })
                .unwrap()
            ))
        );

        assert_eq!(domain_spec("org "), None);
        assert_eq!(domain_spec("1312 "), None);
        assert_eq!(domain_spec(". "), None);
        assert_eq!(domain_spec(".. "), None);
    }

    #[test]
    fn domain_end_ok() {
        assert_eq!(domain_end(".com "), Some((" ", "com")));
        assert_eq!(domain_end(".com. "), Some((" ", "com")));
        assert_eq!(domain_end(".abc-123 "), Some((" ", "abc-123")));
        assert_eq!(domain_end(".123 "), None);
        assert_eq!(
            domain_end(".xexcessivexlabelxlengthxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"),
            None
        );
        assert_eq!(domain_end(" "), None);
    }

    #[test]
    fn toplabel_ok() {
        assert_eq!(toplabel("a1--1a "), Some((" ", "a1--1a")));
        assert_eq!(toplabel("a1--1a-b2 "), Some((" ", "a1--1a-b2")));
        assert_eq!(toplabel("a1-"), Some(("-", "a1")));
        assert_eq!(toplabel("-a"), None);
        assert_eq!(toplabel("2bc "), Some((" ", "2bc")));
        assert_eq!(toplabel("2bc3d "), Some((" ", "2bc3d")));
        assert_eq!(toplabel("bc "), Some((" ", "bc")));
        assert_eq!(toplabel("bc3 "), Some((" ", "bc3")));
        assert_eq!(toplabel("bc3d "), Some((" ", "bc3d")));
        assert_eq!(toplabel("23 "), None);
        assert_eq!(toplabel(" "), None);
    }

    #[test]
    fn explain_string_ok() {
        assert_eq!(
            explain_string("Abc %{d}-x\t"),
            Some((
                "\t",
                ExplainString {
                    segments: vec![
                        ExplainStringSegment::MacroString(MacroString {
                            segments: vec![MacroStringSegment::MacroLiteral(
                                MacroLiteral::new("Abc").unwrap()
                            )],
                        }),
                        ExplainStringSegment::Space,
                        ExplainStringSegment::MacroString(MacroString {
                            segments: vec![
                                MacroStringSegment::MacroExpand(MacroExpand::Macro(Macro::new(
                                    MacroKind::Domain
                                ))),
                                MacroStringSegment::MacroLiteral(MacroLiteral::new("-x").unwrap()),
                            ],
                        }),
                    ],
                }
            ))
        );
        assert_eq!(
            explain_string("\t"),
            Some(("\t", ExplainString { segments: vec![] }))
        );
    }

    #[test]
    fn macro_string_ok() {
        assert_eq!(
            macro_string("%{d}%_Abc.123 "),
            Some((
                " ",
                MacroString {
                    segments: vec![
                        MacroStringSegment::MacroExpand(MacroExpand::Macro(Macro::new(
                            MacroKind::Domain
                        ))),
                        MacroStringSegment::MacroExpand(MacroExpand::Escape(Escape::Space)),
                        MacroStringSegment::MacroLiteral(MacroLiteral::new("Abc.123").unwrap()),
                    ]
                }
            ))
        );
        assert_eq!(
            macro_string(" "),
            Some((" ", MacroString { segments: vec![] }))
        );
    }

    #[test]
    fn macro_expand_ok() {
        assert_eq!(
            macro_expand("%{s} "),
            Some((" ", MacroExpand::Macro(Macro::new(MacroKind::Sender))))
        );
        assert_eq!(
            macro_expand("%{S3R.+} "),
            Some((
                " ",
                MacroExpand::Macro({
                    let mut m = Macro::new(MacroKind::Sender);
                    m.set_url_encode(true);
                    m.set_limit(NonZeroU8::new(3).unwrap());
                    m.set_reverse(true);
                    m.set_delimiters(Delimiters::new(".+").unwrap());
                    m
                })
            ))
        );
        assert_eq!(macro_expand("%{S345} "), None);
        assert_eq!(macro_expand("%{Sr "), None);

        assert_eq!(
            macro_expand("%% "),
            Some((" ", MacroExpand::Escape(Escape::Percent)))
        );
        assert_eq!(macro_expand("%z "), None);

        assert_eq!(macro_expand("% "), None);
        assert_eq!(macro_expand("%{ "), None);
    }

    #[test]
    fn macro_letter_ok() {
        assert_eq!(macro_letter("s "), Some((" ", (MacroKind::Sender, false))));
        assert_eq!(macro_letter("S "), Some((" ", (MacroKind::Sender, true))));
        assert_eq!(macro_letter("x "), None);
    }

    #[test]
    fn macro_transformer_limit_ok() {
        assert_eq!(macro_transformer_limit("0 "), None);
        assert_eq!(macro_transformer_limit("1 "), Some((" ", NonZeroU8::new(1).unwrap())));
        assert_eq!(macro_transformer_limit("123 "), Some((" ", NonZeroU8::new(123).unwrap())));
        assert_eq!(macro_transformer_limit("0123 "), Some((" ", NonZeroU8::new(123).unwrap())));
        assert_eq!(macro_transformer_limit("1230 "), None);
        assert_eq!(macro_transformer_limit(" "), None);
    }

    #[test]
    fn macro_transformer_reverse_ok() {
        assert_eq!(macro_transformer_reverse("R "), Some((" ", true)));
        assert_eq!(macro_transformer_reverse(" "), Some((" ", false)));
    }

    #[test]
    fn macro_delimiters_ok() {
        assert_eq!(
            macro_delimiters("./-- "),
            Some((" ", Delimiters::new("./--").unwrap()))
        );
        assert_eq!(macro_delimiters(" "), None);
        assert_eq!(macro_delimiters("x "), None);
    }

    #[test]
    fn macro_literal_ok() {
        assert_eq!(
            macro_literal("Abc.123.!# "),
            Some((" ", MacroLiteral::new("Abc.123.!#").unwrap()))
        );
        assert_eq!(macro_literal(" "), None);
        assert_eq!(macro_literal("% "), None);
    }
}
