# viaspf-record changelog

## 0.4.1 (2022-05-10)

### Added

* `MacroString` can now be parsed from a string via `std::str::FromStr`.

## 0.4.0 (2022-03-06)

Initial release.

This package was split out from the [viaspf] package, where it used to live as
module `viaspf::record`. The initial version is 0.4.0, the version when the
split took place. From now on, this package and its version number will be
updated independently from viaspf.

### Changed

* Parsing errors are now encoded with the new [`ParseError`] struct.

[viaspf]: https://crates.io/crates/viaspf
[`ParseError`]: https://docs.rs/viaspf-record/0.4.0/viaspf_record/struct.ParseError.html
